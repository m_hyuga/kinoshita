<?php
/**
 * entry point for PHP-OpenCloud library
 */
require_once(dirname(__FILE__) . '/autoload.php');
require_once(dirname(__FILE__) . '/opencloud/globals.php');
$classLoader = new SplClassLoader('opencloud', dirname(__FILE__));
$classLoader->register();
