<?php
wp_enqueue_style("gallerifficcss-css", get_bloginfo('template_directory').'/css/angel_gallery/galleriffic.css' );

wp_enqueue_script("galleriffic",get_bloginfo('template_directory').'/js/angel_gallery/jquery.galleriffic.js' );
wp_enqueue_script("opacityrollover",get_bloginfo('template_directory').'/js/angel_gallery/jquery.opacityrollover.js' );
wp_enqueue_script("myGallerifficjs",get_bloginfo('template_directory').'/js/angel_gallery/jquery.myGalleriffic.js', false, false, true );
	$pageName = 'angels';
?>

<?php 
	/* Template Name: kids-笑顔の天使たち */
	get_header();
?>

	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php site_top_url(); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<a href="<?php site_top_url(); ?>/kids/" title="お子様｜すくすくと成長される子供の節目・記念に☆">お子様</a>&nbsp;＞&nbsp;
		笑顔の天使たち
	</div><!-- End: bcList -->

	<div id="main" class="<?php echo $pageName; ?> Column1">

<?php the_post(); ?>
		<h3 title="お子様｜笑顔の天使たち">笑顔の天使たち</h3>

		<div class="entry post-<?php the_ID(); ?>">
			<?php the_content(); ?>
		</div>		
	</div>


	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php site_top_url(); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<a href="<?php site_top_url(); ?>/kids/" title="お子様｜すくすくと成長される子供の節目・記念に☆">お子様</a>&nbsp;＞&nbsp;
		笑顔の天使たち
	</div><!-- End: bcList -->


<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>