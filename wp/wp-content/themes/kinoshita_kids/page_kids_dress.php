<?php 
	/* Template Name: キッズ衣装 */
	/*wp_enqueue_style("common_1clm_css", get_bloginfo('template_directory').'/css/common_1column.css' );*/
wp_enqueue_style("lightboxcss", get_bloginfo('template_directory').'/css/gallery/jquery.lightbox-0.5.css' );
wp_enqueue_script("lightboxjs",get_bloginfo('template_directory').'/js/gallery/jquery.lightbox-0.5.js' );
	$pageColumn = 1;
	$pageName = dress;
	$base_dir = '/home/sites/heteml/users/k/i/n/kino3/web/images/';
	get_header();
?>

	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php site_top_url(); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<a href="<?php site_top_url(); ?>/kids/" title="お子様｜すくすくと成長される子供の節目・記念に☆">お子様</a>&nbsp;＞&nbsp;
		衣装
	</div><!-- End: bcList -->

	<div id="main" class="Column1 dress kidsDress">
    
<?php the_post(); ?>

		<h3 title="お子様の衣装">お子様の衣装</h3>
		<p class="bCopy">スタジオキノシタでは、撮影時にご利用いただける衣装をたくさんご用意しております。</p>

		<ul class="floatList clearfix" id="tab">
		  <li class="btn01"><a href="#baby" title="赤ちゃんお子様（赤ちゃん）のお宮参り・お食い初め（百日祝い）の撮影用の貸衣装" class="active"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/tab_01_off.gif" width="218" height="46" alt="お子様の衣装｜赤ちゃん"></a></li>
		  <li class="btn02"><a href="#first" title="お子様の初誕生・1歳記念・初節句の写真撮影・１歳撮影用の貸衣装"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/tab_02_off.gif" width="218" height="46" alt="お子様の衣装｜1歳"></a></li>
		  <li class="btn03"><a href="#shichigosan" title="お子様の七五三の撮影用貸衣装（衣装レンタル）"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/tab_03_off.gif" width="218" height="46" alt="お子様の衣装｜七五三"></a></li>
		  <li class="btn04"><a href="#tuxedo" title="お子様のドレス・タキシードなどの撮影用貸衣装（衣装レンタル）"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/tab_04_off.gif" width="218" height="46" alt="お子様の衣装｜ドレス・タキシード"></a></li>
		</ul>
        
		<div class="entry post-<?php the_ID(); ?>" id="baby">

			<p class="textCenter mb15">お宮参り・百日祝い・初節句など、充実した衣装をご覧ください（写真はクリックすると拡大します）</p>
            
			<ol class="clearfix">
			  <li><a href="#baby01" title="お子様の衣装｜男の子"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/baby/icon_01.gif" width="134" height="30" alt="お子様の衣装｜男の子"></a></li>
				<li><a href="#baby02" title="お子様の衣装｜女の子"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/baby/icon_02.gif" width="134" height="30" alt="お子様の衣装｜女の子"></a></li>
				<li><a href="#baby03" title="お子様の衣装｜共通・ドレス"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/baby/icon_03.gif" width="134" height="30" alt="お子様の衣装｜共通・ドレス"></a></li>
				<li><a href="#baby04" title="お子様の衣装｜外出用着物"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/baby/icon_04.gif" width="134" height="30" alt="お子様の衣装｜外出用着物"></a></li>

		  </ol>        

		    <div id="baby01" class="category">
			<h4><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/baby/sttl_01.gif" width="230" height="56" alt="お子様の衣装｜男の子" title="お子様の衣装｜男の子"></h4>
			<ul class="clearfix">

<?php 
$num = count(glob($base_dir."kids_dress/baby/00*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
					<a href="/images/kids_dress/baby/00_<?php echo $i; ?>.jpg" rel="lightbox[00]" title="お子様の衣装｜男の子">
						<img src="/images/kids_dress/baby/00_<?php echo $i; ?>_thumb.jpg" alt="お子様の衣装｜男の子" />
					</a>
				</li>
<?php endfor; ?>
			</ul>
		    </div><!-- /#baby01 -->

  

		    <div class="category"><!-- 男の子 着物(スタジオ撮影用) -->
			<h4><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/baby/sttl_02.gif" width="318" height="56" alt="男の子 着物(スタジオ撮影用）" title="男の子 着物(スタジオ撮影用）"></h4>
			<ul class="clearfix">

<?php $num = count(glob($base_dir."kids_dress/baby/01*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
					<a href="/images/kids_dress/baby/01_<?php echo $i; ?>.jpg" rel="lightbox[01]" title="男の子 着物(スタジオ撮影用)">
						<img src="/images/kids_dress/baby/01_<?php echo $i; ?>_thumb.jpg" alt="男の子 着物(スタジオ撮影用)" />
					</a>
				</li>
<?php endfor; ?>
			</ul>
		    </div><!-- /男の子 着物(スタジオ撮影用) -->

	<div class="pagetop">
		<a href="#page" title="お子様の衣装｜ページTOPへ"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/pagetop.gif" alt="お子様の衣装｜ページTOPへ" width="114" height="42"></a>
	</div>    

		    <div id="baby02" class="category"><!-- /#baby02 -->
			<h4><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/baby/sttl_03.gif" width="230" height="62" alt="お子様の衣装｜女の子" title="お子様の衣装｜女の子"></h4>
			<ul class="clearfix">

<?php $num = count(glob($base_dir."kids_dress/baby/02*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
					<a href="/images/kids_dress/baby/02_<?php echo $i; ?>.jpg" rel="lightbox[02]" title="お子様の衣装｜女の子">
						<img src="/images/kids_dress/baby/02_<?php echo $i; ?>_thumb.jpg" alt="お子様の衣装｜女の子" />
					</a>
				</li>
<?php endfor; ?>
			</ul>
		    </div><!-- /#baby02 -->

		    <div class="category"><!-- 女の子 着物(スタジオ撮影用) -->
			<h4><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/baby/sttl_04.gif" width="310" height="56" alt="女の子 着物(スタジオ撮影用）" title="女の子 着物(スタジオ撮影用）"></h4>
			<ul class="clearfix">

<?php $num = count(glob($base_dir."kids_dress/baby/03*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
					<a href="/images/kids_dress/baby/03_<?php echo $i; ?>.jpg" rel="lightbox[03]" title="女の子 着物(スタジオ撮影用)">
						<img src="/images/kids_dress/baby/03_<?php echo $i; ?>_thumb.jpg" alt="女の子 着物(スタジオ撮影用)" />
					</a>
				</li>
<?php endfor; ?>
			</ul>
		    </div><!-- /女の子 着物(スタジオ撮影用) -->

	<div class="pagetop">
		<a href="#page" title="お子様の衣装｜ページTOPへ"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/pagetop.gif" alt="お子様の衣装｜ページTOPへ" width="114" height="42"></a>
	</div>    


		    <div id="baby03" class="category"><!-- /#baby03 -->
			<h4><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/baby/sttl_05.gif" width="230" height="62" alt="お子様の衣装｜男女共通" title="お子様の衣装｜男女共通"></h4>
			<ul class="clearfix">

<?php $num = count(glob($base_dir."kids_dress/baby/04*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
					<a href="/images/kids_dress/baby/04_<?php echo $i; ?>.jpg" rel="lightbox[04]" title="お子様の衣装｜男女共通">
						<img src="/images/kids_dress/baby/04_<?php echo $i; ?>_thumb.jpg" alt="お子様の衣装｜男女共通" />
					</a>
				</li>
<?php endfor; ?>
			</ul>
		    </div><!-- /#baby03 -->

		    <div class="category"><!-- ドレス -->
			<h4><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/baby/sttl_06.gif" width="232" height="56" alt="お子様の衣装｜ドレス" title="お子様の衣装｜ドレス"></h4>
			<ul class="clearfix">

<?php $num = count(glob($base_dir."kids_dress/baby/05*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
					<a href="/images/kids_dress/baby/05_<?php echo $i; ?>.jpg" rel="lightbox[05]" title="お子様の衣装｜ドレス">
						<img src="/images/kids_dress/baby/05_<?php echo $i; ?>_thumb.jpg" alt="お子様の衣装｜ドレス" />
					</a>
				</li>
<?php endfor; ?>
			</ul>
		    </div><!-- /ドレス -->

	<div class="pagetop">
		<a href="#page" title="お子様の衣装｜ページTOPへ"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/pagetop.gif" alt="お子様の衣装｜ページTOPへ" width="114" height="42"></a>
	</div>    


	      <div id="baby04" class="category"><!-- /#baby04 -->
			<h4><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/baby/sttl_07.gif" width="430" height="56" alt="お出かけ用　着物も各種取りそろえております。" title="お出かけ用　着物も各種取りそろえております。"></h4>
			<ul class="clearfix">

<?php $num = count(glob($base_dir."kids_dress/baby/06*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
				  <a href="/images/kids_dress/baby/06_<?php echo $i; ?>.jpg" rel="lightbox[06]" title="お子様の衣装｜お出かけ用">
						<img src="/images/kids_dress/baby/06_<?php echo $i; ?>_thumb.jpg" alt="お子様の衣装｜お出かけ用" />
				  </a>
				</li>
<?php endfor; ?>
			</ul>

			<ul class="clearfix listend">

<?php $num = count(glob($base_dir."kids_dress/baby/07*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
				  <a href="/images/kids_dress/baby/07_<?php echo $i; ?>.jpg" rel="lightbox[07]" title="お子様の衣装｜お出かけ用">
						<img src="/images/kids_dress/baby/07_<?php echo $i; ?>_thumb.jpg" alt="お子様の衣装｜お出かけ用" />
				  </a>
				</li>
<?php endfor; ?>
			</ul>
            <p class="dressText">当日17:00までにご返却ください。</p>
            <ul class="cautionList clearfix">
              <li>
                <h5 title="お子様の衣装｜衣装が汚れたら"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/sttl_01.png" width="212" height="60" alt="お子様の衣装｜衣装が汚れたら"></h5>
                <p>水をつけたり、強く擦ったりせずそのままで！<br>
                汚れた箇所をお知らせ下さい　程度によりクリーニング代が掛かります。</p>
              </li>
              <li>
                <h5 title="お子様の衣装｜小物類を紛失・破損の場合"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/sttl_02.png" width="212" height="60" alt="お子様の衣装｜小物類を紛失・破損の場合"></h5>
                <p>1セット分のお代が掛かります。</p>
              </li>
            </ul>
            <div class="textRight"><a href="<?php site_top_url(); ?>/kids/omiyamairi/" title="お子様の衣装｜お宮参り・百日祝いのプランはこちら"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/baby/bnr_01.jpg" alt="お子様の衣装｜お宮参り・百日祝いのプランはこちら" width="287" height="108"></a></div>
		    </div><!-- /#baby04 -->
          

	  </div><!-- /entry -->	

		<div class="entry" id="first">
          <p class="textCenter mb15">初めてのお誕生日のお祝いにふさわしい衣装がたくさん！（写真はクリックすると拡大します） </p>

			<ul class="clearfix">

<?php $num = count(glob($base_dir."kids_dress/first/00*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
				  <a href="/images/kids_dress/first/00_<?php echo $i; ?>.jpg" rel="lightbox[08]" title="【1歳】お子様の初誕生・1歳記念・初節句の写真撮影・１歳撮影用の貸衣装">
						<img src="/images/kids_dress/first/00_<?php echo $i; ?>_thumb.jpg" alt="【1歳】お子様の初誕生・1歳記念・初節句の写真撮影・１歳撮影用の貸衣装" />
				  </a>
				</li>
<?php endfor; ?>
			</ul>

            <div class="textRight"><a href="<?php site_top_url(); ?>/kids/first_anniversary/" title="お子様の衣装｜1歳記念のプランはこちら"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/first/bnr_01.jpg" alt="お子様の衣装｜1歳記念のプランはこちら" width="287" height="108"></a></div>
		</div>
		<!-- /first-->	

		<div class="entry" id="shichigosan">
          <p class="textCenter mb15">毎年、新着衣装も入荷！常時280点以上ございます。（写真はクリックすると拡大します）</p>
 
          <dl class="leftArea">
            <dt><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/753/img_01.jpg" alt="衣装ルームには、たくさんの衣装がいっぱい！" width="299" height="267" title="お子様の衣装｜衣装ルームには、たくさんの衣装がいっぱい！"></dt>
            <dd>衣装ルームには、たくさんの衣装がいっぱい！<br>
            お子様に似合う1着をお選びいただけます。</dd>
          </dl>
 
          <div class="rightArea">
            <ul class="clearfix">
              
              <?php $num = count(glob($base_dir."kids_dress/753/00*b.jpg")); ?>
              <?php for($i=1; $i<=$num; $i++): ?>
              <?php if($i < 10) { $i = '0'.$i; } ?>
              <li>
                <a href="/images/kids_dress/753/00_<?php echo $i; ?>.jpg" rel="lightbox[09]" title="【七五三】お子様の七五三の写真撮影・七五三の衣装レンタル・着付け">
                  <img src="/images/kids_dress/753/00_<?php echo $i; ?>_thumb.jpg" alt="【七五三】お子様の七五三の写真撮影・七五三の衣装レンタル・着付け" />
                </a>
                </li>
              <?php endfor; ?>
            </ul>
          </div>
            
            <div class="f-clear textRight"><a href="<?php site_top_url(); ?>/kids/shichigosan/" title="お子様の衣装｜七五三のプランはこちら"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/753/bnr_01.jpg" alt="お子様の衣装｜七五三のプランはこちら" width="287" height="108"></a></div>

		</div><!-- /shichigosan-->	

		<div class="entry" id="tuxedo">
          <p class="textCenter mb15">記念写真・その他発表会などのレンタルなどもおこなっています。（写真はクリックすると拡大します）<br>
          ピアノの発表会などに、ドレス・タキシードの貸出もおこなっております。ぜひご利用ください。</p>

		  <ul class="clearfix">

<?php $num = count(glob($base_dir."kids_dress/dress/00*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
				  <a href="/images/kids_dress/dress/00_<?php echo $i; ?>.jpg" rel="lightbox[10]" title="【ドレス・タキシード】お子様のお誕生日・記念写真・大きめサイズの撮影用衣装レンタル">
						<img src="/images/kids_dress/dress/00_<?php echo $i; ?>_thumb.jpg" alt="【ドレス・タキシード】お子様のお誕生日・記念写真・大きめサイズの撮影用衣装レンタル" />
				  </a>
				</li>
<?php endfor; ?>
			</ul>

             <p class="price">レンタル料金　<strong>9,720円</strong>（1泊2日・クツ付き）　<span>対象となるお子様の身長の目安　90cm～140cm</span></p>
             <p class="dressText">当日17:00までにご返却ください。<br>
             延長1泊につき　＋3,240円</p>
           <ul class="cautionList clearfix">
              <li>
                <h5 title="お子様の衣装｜衣装が汚れたら"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/sttl_01.png" width="212" height="60" alt="お子様の衣装｜衣装が汚れたら"></h5>
                <p>水をつけたり、強く擦ったりせずそのままで！<br>
                汚れた箇所をお知らせ下さい　程度によりクリーニング代が掛かります。</p>
             </li>
              <li>
                <h5 title="お子様の衣装｜小物類を紛失・破損の場合"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/dress/sttl_02.png" width="212" height="60" alt="お子様の衣装｜小物類を紛失・破損の場合"></h5>
                <p>1セット分のお代が掛かります。</p>
              </li>
          </ul>
		</div><!-- /tuxedo-->	

		<div class="entry" id="rental"><!-- rental-->	
			<?php the_content(); ?>
		</div><!-- /rental -->	
	</div>


	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php site_top_url(); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<a href="<?php site_top_url(); ?>/kids/" title="お子様｜すくすくと成長される子供の節目・記念に☆">お子様</a>&nbsp;＞&nbsp;
		衣装
	</div><!-- End: bcList -->


<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>

