<?php 
wp_enqueue_style("fractionslidercss", get_bloginfo('template_directory').'/css/home/fractionslider.css' );
wp_enqueue_style("kids_css", get_bloginfo('template_directory').'/css/kids.css' );

wp_enqueue_script("fractionsliderjs",get_bloginfo('template_directory').'/js/home/jquery.fractionslider.js', false, false, false );
wp_enqueue_script("myfractionsliderjs",get_bloginfo('template_directory').'/js/home/jquery.myfractionslider.js', false, false, true );

	$pageName = 'kids';

get_header(); ?>

	<!-- Start: bcList -->
	<div class="bcList Column2">
		■ 現在位置 : <a href="<?php site_top_url(); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		お子様
	</div><!-- End: bcList -->

    <div id="main" class="kids wrapper Column2">

	<div id="primary" class="site-content">
<div class="copy">
<h3 title="幸せな笑顔、大切な一瞬を。素敵な写真は未来への贈りもの"><span>幸せな笑顔、大切な一瞬を。素敵な写真は未来への贈りもの</span></h3>
<p>すくすくと成長されるお子様のかわいらしい一瞬をスタジオで残しませんか。<br /> 記念日や、ふだんの自然な笑顔を写真に残し未来へのプレゼントにしましょう。</p>
<p>七五三、お宮参り・百日祝い、1歳記念、10歳記念（1/2成人式）、卒入園・卒入学、誕生日は、石川県金沢市のフォトスタジオ キノシタにおまかせください。</p>
<ul class="clearfix">
	<li><a href="<?php site_top_url(); ?>/kids/dress/" title="お子様の衣装・ヘアメイクについて"><img alt="お子様の衣装・ヘアメイクについて" src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/dress_bnr.png" width="288" height="108" /></a></li>
	<li><a href="<?php site_top_url(); ?>/photogallery/" title="お子様のフォトギャラリー"><img alt="お子様のフォトギャラリー" src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/gallery_bnr.png" width="288" height="108" /></a></li>
</ul>
</div>
<!-- /.copy -->
<div class="campaign">
<h4 title="石川県金沢市の写真館「フォトスタジオ キノシタ」キャンペーン　-Campaign-"><span>キャンペーン　-Campaign-</span></h4>
				<ul>
				<li><a href="<?php bloginfo('url'); ?>/kids/shichigosan/"><img src="<?php echo get_template_directory_uri(); ?>/images/banner/shichigosan_hayadori5.jpg" alt="七五三早撮りキャンペーン" /></a></li>
<?php switch_to_blog(1); ?>
<?php 
$today = date("Y/m/d");
$timeout = date("Y/m/d", strtotime("+ 10 days"));

$wp_query = new WP_Query(array(
'meta_query' => array(
/*			array(	'key'=>'開始日',
				'value'=>$today,
				'compare'=>'<=',
				'type'=>'DATE'
				),*/
			array(	'key'=>'終了日',
				'value'=>$timeout,
				'compare'=>'>=',
				'type'=>'DATE'
				),
			'relation'=>'AND'
			),

'post_type' => 'campaign',
'posts_per_page' => '-1',
'paged' => $paged,
'orderby' => 'date',
'order' => 'DESC'
));
if ($wp_query->have_posts()) :
	while($wp_query->have_posts()) : $wp_query->the_post(); ?>

<?php $ctm = get_post_meta($post->ID, 'バナー', true);?>
<?php if(!empty($ctm)):?>
      <!-- Start: Post -->
      <li><a href="<?php echo get_permalink() ?>"><?php bannerL_image ('バナー'); ?></a></li>
    <!-- End: Post -->
<?php endif;?>

<?php endwhile; ?>
<?php else: ?>
<?php endif; ?>
<?php wp_reset_query(); ?>
</ul>
<br>
</div>
<!-- /.campaign -->
<?php switch_to_blog(2); ?>
<?php
$page =  get_page_by_title( 'お子様TOP' );
$page_include = apply_filters( 'the_content',$page->post_content); //ページの本文をフィルターフックで整形してます
echo $page_include; //出力します
?>

<?php wp_reset_query(); ?>
	</div><!-- #primary -->

<?php get_sidebar(); ?>

    </div><!-- #main .wrapper -->

	<!-- Start: bcList -->
	<div class="bcList Column2">
		■ 現在位置 : <a href="<?php site_top_url(); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		お子様
	</div><!-- End: bcList -->

<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>