<?php global $pageName; ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo(stylesheet_url); ?>">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<?php wp_head(); ?>


<?php wp_head(); ?>

<?php if($pageName=='dress'): ?>
	<script>
		$(function() {
			$('a[rel^=lightbox]').lightBox();
		} );
	</script>
<?php endif; ?>


<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>
<!--[if gt IE 6]>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/textshadow.js"></script>
	<style>
		#footer h2 {
			font-family:"ＭＳ Ｐ明朝", "MS PMincho", "ヒラギノ明朝 Pro W3", "Hiragino Mincho Pro", serif;
		}
	</style>
<![endif]-->
<!--[if lt IE 8]>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.pseudo.js"></script>
<![endif]-->
<!--Google Analytics-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33861131-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--Analytics End-->
</head>

<?php if(is_front_page()): ?>
<body class="kidsHome blog logged-in">
<?php else: ?>



<body <?php body_class(); ?>>
<?php endif; ?>

<div id="page" class="hfeed site kids">

	<div id="header">
		<div class="siteInfo clearfix">
			<div class="ttl">
				<h2>七五三、お宮参り・百日祝い、1歳記念、10歳記念（1/2成人式）、卒入園・卒入学、誕生日｜石川県金沢市のフォトスタジオ キノシタ</h2>
				<h1><a href="<?php site_top_url(); ?>/" title="七五三、お宮参り・百日祝い、1歳記念、10歳記念（1/2成人式）、卒入園・卒入学、誕生日は、石川県金沢市のフォトスタジオ キノシタにおまかせください。"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/logo.gif" alt="七五三、お宮参り・百日祝い、1歳記念、10歳記念（1/2成人式）、卒入園・卒入学、誕生日｜石川県金沢市のフォトスタジオ キノシタ" width="286" height="84"></a></h1>

			</div>
			<div class="address">
				<ol>
					<li><a href="https://www.studio-kinoshita.com/contact/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」｜お問い合わせ">お問い合わせ</a>｜</li><!--
					--><li><a href="<?php site_top_url(); ?>/feature/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」｜会社案内">会社案内</a></li>
				</ol>
				<ul>
					<li class="map"><a href="<?php site_top_url(); ?>/access/"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_map.gif" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜MAP（アクセス・地図 ）" width="40" height="40" title="石川県金沢市の写真館「フォトスタジオ キノシタ」｜MAP（アクセス・地図 ）"></a></li>
					<li class="tel">076-244-4649</li>
					<li>石川県金沢市平和町 2-11-11</li>
				</ul>
			</div>
		</div><!-- /.siteInfo -->
		<div class="globalNavi clearfix">
			<ul class="clearfix">
				<li><h3><a href="<?php site_top_url(); ?>/kids/" class="gn01" title="お子様｜すくすくと成長される子供の節目・記念に☆"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_sub_navi01.png" alt="お子様｜すくすくと成長される子供の節目・記念に☆" width="103" height="79"></a></h3></li><!--
				--><li><h3><a href="<?php site_top_url(); ?>/growup/" class="gn02" title="成人式・卒業式写真｜新成人を祝福する「成人の日」の記念に♪"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_sub_navi02.png" alt="成人式・卒業式写真｜新成人を祝福する「成人の日」の記念に♪" width="103" height="79"></a></h3></li><!--
				--><li><h3><a href="<?php site_top_url(); ?>/bridal/" class="gn03" title="結婚式写真・ブライダルフォト｜最高に幸せなウェディング、メモリアル記念♪"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_sub_navi03.png" alt="結婚式写真・ブライダルフォト｜最高に幸せなウェディング、メモリアル記念♪" width="103" height="79"></a></h3></li><!--
				--><li><h3><a href="<?php site_top_url(); ?>/family/" class="gn04" title="家族写真・記念写真｜結婚記念日や敬老の日のお祝い、マタニティフォトなど。"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_sub_navi04.png" alt="家族写真・記念写真｜結婚記念日や敬老の日のお祝い、マタニティフォトなど。" width="103" height="79"></a></h3></li><!--
				--><li><h3><a href="<?php site_top_url(); ?>/portrait/" class="gn05" title="ポートレート・肖像写真｜肖像・プロフィール・オーディションなど。"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_sub_navi05.png" alt="ポートレート・肖像写真｜肖像・プロフィール・オーディションなど。" width="103" height="79"></a></h3></li><!--
				--><li><h3><a href="<?php site_top_url(); ?>/idphoto/" class="gn06" title="証明写真｜第一印象に差をつける！"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_sub_navi06.png" alt="証明写真｜第一印象に差をつける！" width="107" height="79"></a></h3></li>
			</ul>
		</div><!-- /.globalNavi -->

<?php if(is_front_page()): ?>
<div id="kv" class="slider">
<!-- your slider container -->
  <!-- and so on -->
  <div class="fs_loader"><!-- shows a loading .gif while the slider is getting ready --></div>

  <div class="slide">
	<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kv01.jpg">
  </div>
  <div class="slide">
	<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kv02.jpg">
  </div>
  <div class="slide">
	<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kv03.jpg">
  </div>
  <div class="slide">
	<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kv04.jpg">
  </div>
  <div class="slide">
	<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kv05.jpg">
  </div>
  <div class="slide">
	<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kv06.jpg">
  </div>

</div>

<?php else: ?>

		<div id="kv">
<?php if($pageName=='shichigosan'): ?>
			<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kv01.jpg" alt="お子様の七五三の写真撮影・七五三の衣装レンタル・着付け" width="960" height="350" title="お子様の七五三メニュー">
<?php elseif($pageName=='omiyamairi'): ?>
			<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kv02.jpg" alt="お子様（赤ちゃん）のお宮参り・お食い初め（百日祝い）の写真撮影" width="960" height="350" title="お子様のお宮参り・百日祝いメニュー">
<?php elseif($pageName=='first_anniversary'): ?>
			<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kv03.jpg" alt="お子様の初誕生・1歳記念・初節句の写真撮影・１歳撮影用の貸衣装" width="960" height="350" title="お子様の1歳記念メニュー"><?php elseif($pageName=='tenth_anniversary'): ?>
			<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kv04.jpg" alt="お子様の10歳記念（1/2成人式）、十三参りの写真撮影・10歳用の貸衣装" width="960" height="350" title="お子様の10歳記念（1/2成人式）メニュー">
<?php elseif($pageName=='ceremony'): ?>
			<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kv05.jpg" alt="お子様の卒園・入園・卒業・入学の記念写真・卒園ハカマの外出衣装レンタル" width="960" height="350" title="お子様の卒入園・卒入学メニュー">
<?php elseif($pageName=='birthday'): ?>
			<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kv06.jpg" alt="お子様のお誕生日・記念写真・大きめサイズの撮影用衣装レンタル" width="960" height="350" title="お子様のお誕生日メニュー"><?php endif; ?>
		</div>

<?php endif; ?>

		<!-- ロールオーバー画像のload -->
		<div class="preload">
			<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi01_on.png">
			<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi02_on.png">
			<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi03_on.png">
			<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi04_on.png">
			<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi05_on.png">
			<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi06_on.png">
		</div>
		<!-- //ロールオーバー画像のload ここまで -->

		<div class="subNaviArea" id="kidsMenu">
				<ul class="clearfix">
<?php if($pageName=='shichigosan'):?>
					<li><h3><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi01.png" alt="お子様の七五三の写真撮影・七五三の衣装レンタル・着付け" title="お子様の七五三メニュー" class="on"></h3></li><!--
<?php else: ?>
					<li><h3><a href="<?php site_top_url(); ?>/kids/shichigosan/" title="お子様の七五三メニュー"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi01_off.png" alt="お子様の七五三の写真撮影・七五三の衣装レンタル・着付け"></a></h3></li><!--
<?php endif; ?>

<?php if($pageName=='omiyamairi'):?>
					--><li><h3><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi02.png" alt="お子様（赤ちゃん）のお宮参り・お食い初め（百日祝い）の写真撮影" title="お子様のお宮参り・百日祝いメニュー" class="on"></h3></li><!--
<?php else: ?>
					--><li><h3><a href="<?php site_top_url(); ?>/kids/omiyamairi/" title="お子様のお宮参り・百日祝いメニュー"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi02_off.png" alt="お子様（赤ちゃん）のお宮参り・お食い初め（百日祝い）の写真撮影"></a></h3></li><!--
<?php endif; ?>

<?php if($pageName=='first_anniversary'):?>
					--><li><h3><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi03.png" alt="お子様の初誕生・1歳記念・初節句の写真撮影・１歳撮影用の貸衣装" title="お子様の1歳記念メニュー" class="on"></h3></li><!--
<?php else: ?>
					--><li><h3><a href="<?php site_top_url(); ?>/kids/first_anniversary/" title="お子様の1歳記念メニュー"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi03_off.png" alt="お子様の初誕生・1歳記念・初節句の写真撮影・１歳撮影用の貸衣装"></a></h3></li><!--
<?php endif; ?>

<?php if($pageName=='tenth_anniversary'):?>
					--><li><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi04.png" alt="お子様の10歳記念（1/2成人式）、十三参りの写真撮影・10歳用の貸衣装" title="お子様の10歳記念（1/2成人式）メニュー" class="on"></li><!--
<?php else: ?>
					--><li><a href="<?php site_top_url(); ?>/kids/tenth_anniversary/" title="お子様の10歳記念（1/2成人式）メニュー"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi04_off.png" alt="お子様の10歳記念（1/2成人式）、十三参りの写真撮影・10歳用の貸衣装"></a></li><!--
<?php endif; ?>

<?php if($pageName=='ceremony'):?>
					--><li><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi05.png" alt="お子様の卒園・入園・卒業・入学の記念写真・卒園ハカマの外出衣装レンタル" title="お子様の卒入園・卒入学メニュー" class="on"></li><!--
<?php else: ?>
					--><li><a href="<?php site_top_url(); ?>/kids/ceremony/" title="お子様の卒入園・卒入学メニュー"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi05_off.png" alt="お子様の卒園・入園・卒業・入学の記念写真・卒園ハカマの外出衣装レンタル"></a></li><!--
<?php endif; ?>

<?php if($pageName=='birthday'):?>
					--><li><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi06.png" alt="お子様のお誕生日・記念写真・大きめサイズの撮影用衣装レンタル" title="お子様のお誕生日メニュー" class="on"></li>
<?php else: ?>
					--><li><a href="<?php site_top_url(); ?>/kids/birthday/" title="お子様のお誕生日メニュー"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home/kids_navi06_off.png" alt="お子様のお誕生日・記念写真・大きめサイズの撮影用衣装レンタル"></a></li>
<?php endif; ?>
				</ul>
		</div><!-- /.subNaviArea -->

	</div><!-- #header -->