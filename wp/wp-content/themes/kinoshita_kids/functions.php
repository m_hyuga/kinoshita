<?php


/* 不要メニュー項目非表示
--------------------------------------------------------- */
function remove_menu() {
remove_menu_page('edit.php?post_type=daily');
remove_menu_page('edit.php?post_type=campaign');
remove_menu_page('edit.php?post_type=voice');
remove_menu_page('edit.php?post_type=faq');
}
add_action('admin_menu', 'remove_menu');


/* TOPキャンペーンバナー表示
--------------------------------------------------------- */
function shortcode_campaign_bnr() {
	$today = date("Y/m/d");
	$timeout = date("Y/m/d", strtotime("+ 10 days"));
	$source = '';

	switch_to_blog(1);

$wp_query = new WP_Query(array(
'meta_query' => array(
			array(	'key'=>'カテゴリ',
				'value'=>'お子様',
				'compare'=>'='
				),
			/*array(	'key'=>'開始日',
				'value'=>$today,
				'compare'=>'<=',
				'type'=>'DATE'
				),*/
			array(	'key'=>'終了日',
				'value'=>$timeout,
				'compare'=>'>=',
				'type'=>'DATE'
				),
			'relation'=>'AND'
			),
'post_type' => 'campaign',
'posts_per_page' => '-1',
'orderby' => 'date',
'order' => 'DESC'
));

if ($wp_query->have_posts()) :
	while($wp_query->have_posts()) : $wp_query->the_post();
		$ctm = get_post_meta(get_the_ID(), 'バナー', true);
		if(!empty($ctm)):
			$bnr = wp_get_attachment_image(post_custom('バナー'),'bannerL', false, array( 'alt'=> get_the_title( ),  'title'=> get_the_title( )) );
			$source .='<li><a href="'.get_permalink().'">'.$bnr.'</a></li>';
		endif;

	endwhile;
else:
      $source .='<li>ただいまキャンペーンはありません。</li>';
endif;
wp_reset_query();

restore_current_blog();

	return $source;
}
add_shortcode('campaign_bnr', 'shortcode_campaign_bnr');

function shortcode_angels_tag() {

print<<<EOF
	<div id="gallery" class="content">
	<div id="controls" class="controls"></div>
	<div class="slideshow-container">
	<div id="loading" class="loader"></div>
	<div id="slideshow" class="slideshow"></div>
	</div>
	<div id="caption" class="caption-container"></div>
	</div><!--/#gallery-->
EOF;

}
add_shortcode('angels_tag', 'shortcode_angels_tag');




?>