<?php 
wp_enqueue_style("gallerifficcss", get_bloginfo('template_directory').'/css/home/galleriffic.css' );
wp_enqueue_style("fractionslidercss", get_bloginfo('template_directory').'/css/home/fractionslider.css' );

wp_enqueue_script("gallerifficjs",get_bloginfo('template_directory').'/js/home/jquery.galleriffic.js', false, false, false );
wp_enqueue_script("opacityrolloverjs",get_bloginfo('template_directory').'/js/home/jquery.opacityrollover.js', false, false, false );
wp_enqueue_script("myGallerifficjs",get_bloginfo('template_directory').'/js/home/jquery.myGalleriffic.js', false, false, true );
wp_enqueue_script("fractionsliderjs",get_bloginfo('template_directory').'/js/home/jquery.fractionslider.js', false, false, false );
wp_enqueue_script("myfractionsliderjs",get_bloginfo('template_directory').'/js/home/jquery.myfractionslider.js', false, false, true );

get_header(); ?>

    <div id="main" class="wrapper">

	<div id="content" role="main" class="clearfix">
		<div id="primary" class="site-content">
			<div class="campaign">
				<h4 title="石川県金沢市の写真館「フォトスタジオ キノシタ」｜キャンペーン　-Campaign-"><span>キャンペーン　-Campaign-</span></h4>


				<ul>
						<li><a href="<?php bloginfo('url'); ?>/kids/shichigosan/"><img src="<?php echo get_template_directory_uri(); ?>/images/banner/shichigosan_hayadori5.jpg" alt="七五三早撮りキャンペーン" /></a></li>
<?php 
$today = date("Y/m/d");
$timeout = date("Y/m/d", strtotime("+ 10 days"));

$wp_query = new WP_Query(array(
'meta_query' => array(
/*			array(	'key'=>'開始日',
				'value'=>$today,
				'compare'=>'<=',
				'type'=>'DATE'
				),*/
			array(	'key'=>'終了日',
				'value'=>$timeout,
				'compare'=>'>=',
				'type'=>'DATE'
				),
			'relation'=>'AND'
			),

'post_type' => 'campaign',
'posts_per_page' => '-1',
'paged' => $paged,
'orderby' => 'date',
'order' => 'DESC'
));
if ($wp_query->have_posts()) :
	while($wp_query->have_posts()) : $wp_query->the_post(); ?>

<?php $ctm = get_post_meta($post->ID, 'バナー', true);?>
<?php if(!empty($ctm)):?>
      <!-- Start: Post -->
      <li><a href="<?php echo get_permalink() ?>"><?php bannerL_image ('バナー'); ?></a></li>
    <!-- End: Post -->
<?php endif;?>

<?php endwhile; ?>
<?php else: ?>
<?php endif; ?>
<?php wp_reset_query(); ?>

				</ul>

			</div>
			<br>
			<br>
			<div class="gallery">
				<h4 title="石川県金沢市の写真館「フォトスタジオ キノシタ」｜フォトギャラリー　-Photo Gallery-"><span>フォトギャラリー　-Photo Gallery-</span></h4>
				<div class="galleryInner">
					<div class="slideshow-container">
						<div id="loading" class="loader"></div>
						<div id="slideshow" class="slideshow"></div>
					</div>
					<div id="thumbs" class="navigation">
					  <ul class="thumbs clearfix">
						<li><a class="thumb" name="leaf" href="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo01.jpg" title="石川県金沢市の写真館キノシタ｜フォトギャラリー｜七五三写真"><img src="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo01.jpg" alt="「フォトスタジオ キノシタ」のフォトギャラリー｜七五三写真" /></a></li><!--
						--><li><a class="thumb" name="leaf" href="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo02.jpg" title="石川県金沢市の写真館キノシタ｜フォトギャラリー｜家族写真・記念写真"><img src="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo02.jpg" alt="「フォトスタジオ キノシタ」のフォトギャラリー｜家族写真・記念写真" /></a></li><!--
						--><li><a class="thumb" name="leaf" href="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo03.jpg" title="石川県金沢市の写真館キノシタ｜フォトギャラリー｜入園・卒園／入学・卒業写真"><img src="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo03.jpg" alt="「フォトスタジオ キノシタ」のフォトギャラリー｜入園・卒園／入学・卒業写真" /></a></li><!--
						--><li><a class="thumb" name="leaf" href="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo04.jpg" title="石川県金沢市の写真館キノシタ｜フォトギャラリー｜お誕生日写真"><img src="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo04.jpg" alt="「フォトスタジオ キノシタ」のフォトギャラリーお誕生日写真" /></a></li><!--
						--><li><a class="thumb" name="leaf" href="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo05.jpg" title="石川県金沢市の写真館キノシタ｜フォトギャラリー｜お宮参り・百日祝い写真"><img src="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo05.jpg" alt="「フォトスタジオ キノシタ」のフォトギャラリー｜お宮参り・百日祝い写真" /></a></li><!--
						--><li><a class="thumb" name="leaf" href="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo06.jpg" title="石川県金沢市の写真館キノシタ｜フォトギャラリー｜1歳記念写真"><img src="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo06.jpg" alt="「フォトスタジオ キノシタ」のフォトギャラリー｜1歳記念写真" /></a></li><!--
						--><li><a class="thumb" name="leaf" href="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo07.jpg" title="石川県金沢市の写真館キノシタ｜フォトギャラリー｜成人式・卒業式写真"><img src="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo07.jpg" alt="「フォトスタジオ キノシタ」のフォトギャラリー｜成人式・卒業式写真" /></a></li><!--
						--><li><a class="thumb" name="leaf" href="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo08.jpg" title="石川県金沢市の写真館キノシタ｜フォトギャラリー｜家族写真・記念写真"><img src="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo08.jpg" alt="「フォトスタジオ キノシタ」のフォトギャラリー｜家族写真・記念写真" /></a></li><!--
						--><li><a class="thumb" name="leaf" href="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo09.jpg" title="石川県金沢市の写真館キノシタ｜フォトギャラリー｜結婚式写真・ブライダルフォト"><img src="<?php echo get_template_directory_uri(); ?>/images/home/gallery/photo09.jpg" alt="「フォトスタジオ キノシタ」のフォトギャラリー｜結婚式写真・ブライダルフォト" /></a></li>
					  </ul>
					</div>
					<p><img src="<?php echo get_template_directory_uri(); ?>/images/home/gallery_deco.png" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜フォトギャラリー　-Photo Gallery-" width="131" height="57"></p>
				</div>
				<p class="more clearfix"><a href="photogallery/" title="「フォトスタジオ キノシタ」のフォトギャラリーをもっと見る！">もっと見る&gt;&gt;</a></p>

			</div>



		</div><!-- #primary -->

<?php get_sidebar(); ?>

	</div><!-- #content -->

	
	<div class="topics">
		<h4 title="石川県金沢市の写真館「フォトスタジオ キノシタ」｜おしらせ　-Topics-"><span>おしらせ　-Topics-</span></h4>
		<p class="rss"><a href="<?php bloginfo('url'); ?>/feed/" title="RSS"><img src="<?php echo get_template_directory_uri(); ?>/images/home/topics_rss.gif" width="29" height="30"></a></p>

		<ul class="clearfix">
<?php $topics = new WP_Query(array(
'posts_per_page' => 5,
'orderby' => 'date',
'order' => 'DESC'
));
if ($topics->have_posts()) :
	while($topics->have_posts()) : $topics->the_post(); ?>
<?php //newマークを2週間付与
$days=14;
$today=date('U'); $entry=get_the_time('U');
$diff1=date('U',($today - $entry))/86400;
?>
			<li><dl class="clearfix"><dt<? if ($days > $diff1) { echo ' class="new"';} ?>><a href="<?php the_permalink(); ?>"><?php echo get_post_meta($post->ID,'リード文',true); ?></a></dt><dd><?php the_time('m月d日'); ?></dd></dl></li>
	<?php endwhile; ?>
<?php endif; ?>
		</ul>

		<p class="more"><a href="<?php bloginfo('url'); ?>/news/">もっと見る&gt;&gt;</a></p>

	</div><!-- .topics -->


	<div class="scheduleWrapper" id="schedule">

		<iframe src="<?php bloginfo('url'); ?>/calendar/" width="421" height="375" border="0" frameborder="0" scrolling="no" marginwidth="0" marginheight="0" noresize></iframe>

	</div>

    </div><!-- #main .wrapper -->

<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>