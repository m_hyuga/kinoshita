<?php global $pageName, $topics, $campaign; ?>
<?php if ( is_category() ):
	$cat_info = get_category( $cat );
	$myCat = $cat_info->slug;
endif; ?>
		<div id="secondary" class="widget-area">
<?php if($pageName=='topics'):
	require("topics_include.php");
endif; ?>
<?php if($pageName=='campaign'):
	require("campaign_include.php");
endif; ?>
			<ul>

<?php if(!is_front_page()|| $pageName == 'kids'): ?>
				<li><a href="https://www.studio-kinoshita.com/contact/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」撮影のご予約・お問い合わせは076-244-4649またはこちらから"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/side_bnr_contact.png" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」撮影のご予約・お問い合わせ" width="252" height="188"></a></li>
<?php endif; ?>

<?php if( !is_date() == 'false' &&  $pageName != 'topics' ): ?>
				<li><h5><a href="<?php site_top_url(); ?>/plan/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」撮影プラン"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/side_bnr01.png" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」撮影プラン" width="254" height="106"></a></h5></li>
				<li><h5><a href="<?php site_top_url(); ?>/price/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」料金表"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/side_bnr02.png" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」料金表" width="254" height="106"></a></h5></li>
				<li>
					<ul id="tag">
						<li><h5><a href="<?php site_top_url(); ?>/feature/" title="石川県金沢市の写真館｜キノシタって"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/side_tag01.png" alt="石川県金沢市の写真館｜キノシタって" width="261" height="76"></a></h5></li>
						<li><h5><a href="<?php site_top_url(); ?>/flow/" title="「フォトスタジオ キノシタ」の撮影の流れ"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/side_tag02.png" alt="「フォトスタジオ キノシタ」の撮影の流れ" width="261" height="82"></a></h5></li>
						<li><h5><a href="<?php site_top_url(); ?>/faq/" title="石川県金沢市のスタジオ キノシタ【よくある質問】"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/side_tag03.png" alt="石川県金沢市のスタジオ キノシタ【よくある質問】" width="261" height="72"></a></h5></li>
						<li><h5><a href="<?php site_top_url(); ?>/voice/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」お客様の声"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/side_tag04.png" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」お客様の声" width="261" height="79"></a></h5></li>
					</ul>
				</li>
				<li><h5><a href="<?php site_top_url(); ?>/kids/angels/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」笑顔の天使たち"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/side_bnr03.png" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」笑顔の天使たち" width="254" height="106"></a></h5></li>
				<li><h5><a href="<?php site_top_url(); ?>/family/beautyphoto/" title="夢ビューティーフォト～若返り変身写真～"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/side_bnr05.png" alt="夢ビューティーフォト～若返り変身写真～" width="254" height="105"></a></h5></li>
				<li id="freeBnr">
<?php
switch_to_blog(1);
$page =  get_page_by_path( 'sidebanner' );
$page_include = apply_filters( 'the_content',$page->post_content); 
echo $page_include; 
restore_current_blog();

?>
				</li>
<?php endif; ?>
			</ul>


<?php if( (is_front_page() != 'false' && $topics != 'topics_single') || $pageName == 'kids' ): ?>
			<p><a href="<?php site_top_url(); ?>/access/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」｜MAP（アクセス・地図 ）"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/side_map.png" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜MAP（アクセス・地図 ）" width="257" height="236"></a></p>
			<div class="scheduleWrapperMini" id="schedule">

				<iframe src="<?php site_top_url(); ?>/calendar/" width="260" height="220" border="0" frameborder="0" scrolling="no" marginwidth="0" marginheight="0" noresize></iframe>

			</div>
<?php endif; ?>


		</div><!-- #secondary -->
