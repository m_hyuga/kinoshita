<?php 
	/* Template Name: 1カラム用 */
	$pageColumn = 1;
	$pageName = esc_attr( $post->post_name );
	wp_enqueue_style("common_1clm_css", get_bloginfo('template_directory').'/css/common_1column.css' );

	get_header();
?>

	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php site_top_url(); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<?php the_title(); ?>
	</div><!-- End: bcList -->

	<div id="main" class="<?php echo $pageName; ?> Column1">

<?php the_post(); ?>
<?php if($pageName == 'privacy'): ?>
		<?php //プライバシーポリシーだけh3見出し文字がページタイトルと異なる ?>
		<h3 title="プライバシーポリシー ｜個人情報の取り扱いについて">個人情報の取り扱いについて</h3>
<?php elseif($pageName == 'sitemap'): ?>
		<?php //サイトマップはページ上部は撮影メニューという見出しである ?>
		<h3 title="サイトマップ｜撮影メニュー">撮影メニュー</h3>
<?php else: ?>
		<h3 title="<?php the_title(); ?>"><?php the_title(); ?></h3>
<?php endif; ?>
		<div class="entry post-<?php the_ID(); ?>">
			<?php the_content(); ?>
		</div>		
	</div>


	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php site_top_url(); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<?php the_title(); ?>
	</div><!-- End: bcList -->


<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>