<?php 
	/* Template Name: カレンダー */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title>石川県金沢市の写真館スタジオキノシタ｜営業カレンダー　-Schedule-</title>
<link rel="stylesheet" type="text/css" href="<?php bloginfo(stylesheet_url); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory') ?>/css/calendar.css">

<link href='http://fonts.googleapis.com/css?family=Telex' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<?php wp_head(); ?>

<script>
var scSize = $('#schedule',parent.document).attr('class');
$(function(){
	if( scSize.indexOf('Mini') != -1){
		$("#scSize").addClass("scheduleMini");
	}
});
</script>
</head>

<body>
	<div class="schedule" id="scSize">
		<h3 title="石川県金沢市の写真館スタジオキノシタ｜営業カレンダー　-Schedule-">営業カレンダー　-Schedule-</h3>
		<div class="inner">
			<div class="calendar">
				<?php	//営業カレンダー表示
				open_get_calendar(); ?>
			</div>
		</div>
	</div>
</body>

</html>
