<?php 
	/* Template Name: キャンペーン */
	wp_enqueue_style("topics_campaign_css", get_bloginfo('template_directory').'/css/topics_campaign.css' );
	$pageColumn = 2;
	$pageName = 'campaign';
	if( $_REQUEST['show']){
		$show= $_REQUEST['show'];
	}
	get_header();
?>
	<!-- Start: bcList -->
	<div class="bcList Column2">
		■ 現在位置 : <a href="http://www.studio-kinoshita.com/" title="石川県金沢市の写真館スタジオキノシタ｜トップ">トップ</a>&nbsp;＞&nbsp;
<?php if($show == 'all'): ?>
		<a href="http://www.studio-kinoshita.com/campaign/" title="石川県金沢市の写真館スタジオキノシタ｜キャンペーン一覧">キャンペーン</a>&nbsp;＞&nbsp;
		お得なキャンペーン一覧
<?php elseif($show): ?>
		<a href="http://www.studio-kinoshita.com/campaign/" title="石川県金沢市の写真館スタジオキノシタ｜お得なキャンペーン">キャンペーン</a>&nbsp;＞&nbsp;
		<?php echo $show; ?>年のお得なキャンペーン
<?php else: ?>
		キャンペーン
<?php endif; ?>
	</div><!-- End: bcList -->

<div id="main" class="wrapper Column2 campaign campaign-archive">

  <div id="primary">
  
	<h3 title="キャンペーン"><span>キャンペーン</span></h3>

<?php
$camp_Cate["お子様"] = "02";
$camp_Cate["成人・卒業"] = "03";
$camp_Cate["ブライダル"] = "04";
$camp_Cate["家族・記念日"] = "05";
$camp_Cate["ポートレート"] = "06";
$camp_Cate["証明写真"] = "07";
$camp_Cate["その他"] = "08";

$todayCustom = date("Y/m/d");
$timeout = date("Y/m/d", strtotime("+ 10 days"));

if($show == 'all'):
$paged = get_query_var('paged'); //現在のページ番号取得
$campaign= new WP_Query(array(
'post_type' => 'campaign',
'posts_per_page' => 10,
'paged' => $paged,
'orderby' => 'date',
'order' => 'DESC'
));
elseif($show):
$paged = get_query_var('paged'); //現在のページ番号取得
$campaign= new WP_Query(array(
'post_type' => 'campaign',
'year' => intval($show),
'posts_per_page' => 10,
'paged' => $paged,
'orderby' => 'date',
'order' => 'DESC'
));
else:
$campaign= new WP_Query(array(
'meta_query' => array(
/*			array(	'key'=>'開始日',
				'value'=>$todayCustom,
				'compare'=>'<=',
				'type'=>'DATE'
				),*/
			array(	'key'=>'終了日',
				'value'=>$timeout,
				'compare'=>'>=',
				'type'=>'DATE'
				),
			'relation'=>'AND'
			),
'post_type' => 'campaign',
'posts_per_page' => -1,
'orderby' => 'date',
'order' => 'DESC'
));
endif;

if ($campaign->have_posts()) :
	while($campaign->have_posts()) : $campaign->the_post(); ?>
<?php //newマークを2週間付与
$days=30;
$today=date('U'); $entry=get_the_time('U');
$diff1=date('U',($today - $entry))/86400;
//終了後は終了マークを付与（比較用の終了日取得）
$endDay = get_post_meta($post->ID,'終了日',true);
?>

	<!-- Start: Post -->
	<div class="post section">
  <ul class="clearfix">
    <li class="icon"><img alt="<?php echo get_post_meta( $post->ID , 'カテゴリ' , ture ); ?>"  title="<?php echo get_post_meta( $post->ID , 'カテゴリ' , ture ); ?>" src="<?php echo get_bloginfo('template_directory') ?>/images/campaign/icon_<?php echo $camp_Cate[get_post_meta( $post->ID , 'カテゴリ' , ture )]; ?>.gif" /></li>
    <li>
	<dl>
	<dt>
<?php if ($days > $diff1) { echo '<img src="'http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/campaign/new.gif" alt="NEW" title="NEW" />';}//newマーク
if (strtotime($todayCustom) >= strtotime($endDay)) { echo '<img src="'http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/campaign/end.gif" alt="終了" title="終了" />';}//終了マーク
?>
<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></dt>
			
	<dd class="entry clearfix">
		<p>
<?php
$content = get_post_meta($post->ID,'リード文',true)." ";
$content .= strip_tags($post-> post_content);
$content = mb_substr($content, 0, 150);
echo $content;
?>
	<span class="detail"> <a href="<?php the_permalink(); ?>">続きを読む...</a></span>
	<p class="date"><?php the_time('Y年n月j日'); ?></p>
	</dl> 
    </li>
  </ul>
    </div>
    <!-- End: Post -->

<?php endwhile; ?>
	<div class="navigation clearfix">
		<p class="next"><?php next_posts_link('&nbsp;次のページ'); ?></p>
		<p class="previous"><?php previous_posts_link('前のページ&nbsp;'); ?></p>
	</div>
<?php endif; ?>
 
<?php get_template_part( 'primary_footer' ); ?>

  </div>

<?php get_sidebar(); ?>
		

</div>
	<!-- Start: bcList -->
	<div class="bcList Column2">
		■ 現在位置 : <a href="http://www.studio-kinoshita.com/" title="石川県金沢市の写真館スタジオキノシタ｜トップ">トップ</a>&nbsp;＞&nbsp;
<?php if($show == 'all'): ?>
		<a href="http://www.studio-kinoshita.com/campaign/" title="石川県金沢市の写真館スタジオキノシタ｜キャンペーン一覧">キャンペーン</a>&nbsp;＞&nbsp;
		お得なキャンペーン一覧
<?php elseif($show): ?>
		<a href="http://www.studio-kinoshita.com/campaign/" title="石川県金沢市の写真館スタジオキノシタ｜お得なキャンペーン">キャンペーン</a>&nbsp;＞&nbsp;
		<?php echo $show; ?>年のお得なキャンペーン
<?php else: ?>
		キャンペーン
<?php endif; ?>
	</div><!-- End: bcList -->

<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>