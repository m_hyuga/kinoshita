<?php 
	/* Template Name: 準備中用 */
	wp_enqueue_style("common_1clm_css", get_bloginfo('template_directory').'/css/common_1column.css' );
	$pageColumn = 1;
	$pageName = esc_attr( $post->post_name );
	get_header();
?>

	<!-- Start: bcList -->
	<div class="bcList">
		<a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<?php the_title(); ?>
	</div><!-- End: bcList -->

	<div id="main" class="<?php echo $pageName; ?> Column1">

<?php the_post(); ?>

		<div class="entry post-<?php the_ID(); ?>">
			<div class="prepare">
				<img src="<?php echo get_bloginfo('template_directory') ?>/images/common/prepare.png" width="510" height="570" alt="もうすぐ公開！ ちょっと待ってね" title="もうすぐ公開！ ちょっと待ってね">
			</div>
		</div>		
	</div>



<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>