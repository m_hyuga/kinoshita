<?php
	/* Template Name: お客様の声詳細 */
	wp_enqueue_style("voice_css", get_bloginfo('template_directory').'/css/voice.css' );
	$pageName = 'voice';
	$voice= 'voice_single';
get_header(); ?>

	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<a href="<?php bloginfo('url'); ?>/voice/" title="フォトスタジオ キノシタのお客様の声">お客様の声</a>&nbsp;＞&nbsp;<?php echo get_post_meta( $post->ID , 'お住まいのエリア' , ture ); ?>　<?php echo esc_attr( $post->post_title ); ?> 様
	</div><!-- End: bcList -->

<div id="main" class="wrapper Column2 voice">
  <div id="primary">

	<h3 title="フォトスタジオ キノシタのお客様の声"><span>お客様の声</span></h3>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
$value = get_post_meta($post->ID,'撮影日',true);
$ymd = date('Y年m月d日',strtotime($value));

$ques1 = get_post_meta($post->ID,'質問1',true);
$ques2 = get_post_meta($post->ID,'質問2',true);
$ques3 = get_post_meta($post->ID,'質問3',true);

if(!$ques1){$ques1 = 'スタジオキノシタを選んだ理由をお聞かせください';}
if(!$ques2){$ques2 = 'スタジオキノシタで撮影した感想はいかがでしたか';}
if(!$ques3){$ques3 = 'スタジオキノシタからのメッセージ';}
?>
	<div class="post">
        <dl>
        	<dt><?php echo get_post_meta( $post->ID , 'お住まいのエリア' , ture ); ?>　<?php the_title(); ?> 様</dt>
         	<dd class="date"><?php echo $ymd; ?>　撮影</dd>
		<dd>ご利用サービス：<?php echo get_post_meta($post->ID,'ご利用サービス',true); ?></dd>
        </dl>
	</div><!-- .post -->
	<div class="eyechatch" style="width:<?php voice_eyechatch_sizeW (); ?>px;">
		<div class="eyechatchfoot">
			<div class="eyechatchInner"><?php voice_eyechatch_image2 (); ?></div>
		</div>
	</div>

	<h4 class="sttl"><?php echo $ques1 ; ?></h4>
	<ul class="clearfix">
		<li class="qesPhoto"><p class="<?php echo voice_eyechatch_size ('写真1'); ?>"><span><?php voice_eyechatch_image3 ('写真1'); ?></span></p></li>
		<li class="qestext"><p><?php echo nl2br(get_post_meta($post->ID,'選んだ理由について',true)); ?></p></li>
	</ul>

	<h4 class="sttl"><?php echo $ques2 ; ?></h4>
	<ul class="clearfix">
		<li class="qesPhoto"><p class="<?php echo voice_eyechatch_size ('写真2'); ?>"><span><?php voice_eyechatch_image3 ('写真2'); ?></span></p></li>
		<li class="qestext"><p><?php echo nl2br(get_post_meta($post->ID,'撮影の感想',true)); ?></p></li>
	</ul>

<?php if(get_post_meta($post->ID,'メッセージ',true)): ?>
	<h4 class="sttl"><?php echo $ques3 ; ?></h4>
	<ul class="clearfix">
		<li class="qesPhoto"><p class="<?php echo voice_eyechatch_size ('写真3'); ?>"><span><?php voice_eyechatch_image3 ('写真3'); ?></span></li>
		<li class="qestext"><p><?php echo nl2br(get_post_meta($post->ID,'メッセージ',true)); ?></p></li>
	</ul>
<?php endif; ?>


<?php
//ご利用
$pCateArr = array("お子様" => "kids", "成人・卒業" => "growup",  "ブライダル"=> "dridal", "家族・記念日" => "family", "肖像・ポートレート" => "portrait", "証明写真" => "idphoto", "その他" => "other");

$pCate = get_post_meta($post->ID,'カテゴリ',true);
$pCateSlug = $pCateArr[$pCate];
$service = get_post_meta($post->ID,'ご利用サービス',true);

$planUrl = get_bloginfo(url).'/'.$pCateSlug.'/';

if($pCateSlug=='growup'||$pCateSlug=='dridal'):
  if(strstr($service, 'ベーシック')){
  	$anchor = '#basic';
  }else{
  	$planUrl .= 'plan/';

	if(strstr($service, '特割')){ $anchor = '#c01'; }
	elseif(strstr($service, 'アクリル')){ $anchor = '#c02'; }
	elseif(strstr($service, '男子')&&strstr($service, '前撮')){ $anchor = '#c03'; }
	elseif(strstr($service, '男子')&&strstr($service, '撮影')){ $anchor = '#c04'; }
	elseif(strstr($service, '男子')&&strstr($service, '当日')){ $anchor = '#c05'; }
	elseif(strstr($service, '成人')&&strstr($service, '撮影')){ $anchor = '#c06'; }
	elseif(strstr($service, '成人')&&strstr($service, '当日')){ $anchor = '#c07'; }
	elseif(strstr($service, '成人')&&strstr($service, '衣装')){ $anchor = '#c08'; }
	elseif(strstr($service, 'フル')){ $anchor = '#wy03'; }
	elseif(strstr($service, '和洋')){ $anchor = '#wy02'; }
	elseif(strstr($service, '洋')&&(strstr($service, '1')||strstr($service, '１'))){ $anchor = '#y01'; }
	elseif(strstr($service, '和')&&(strstr($service, '1')||strstr($service, '１'))){ $anchor = '#w01'; }
	elseif(strstr($service, '洋')&&(strstr($service, '2')||strstr($service, '２'))){ $anchor = '#y02'; }
	elseif(strstr($service, '和')&&(strstr($service, '2')||strstr($service, '２'))){ $anchor = '#w02'; }
	else{ $anchor = '#recommend'; }
  }
elseif($pCateSlug=='family'):
	if(strstr($service, 'ベーシック')){ $anchor = '#basic'; }
	elseif(strstr($service, 'マタニティ')){ $anchor = '#c01'; }
	else{ $anchor = '#recommend'; }

elseif($pCateSlug=='portrait'):
	if(strstr($service, 'プロフィール')||strstr($service, '肖像')){ $anchor = '#c01'; }
	elseif(strstr($service, 'オーディション')){ $anchor = '#c02'; }
	elseif(strstr($service, '稽古')){ $anchor = '#c03'; }
	else{ $anchor = '#recommend'; }

elseif($pCateSlug=='idphoto'):
	if(strstr($service, 'デジタル')){ $anchor = '#c01'; }
	elseif(strstr($service, 'プロ')){ $anchor = '#c02'; }
	else{ $anchor = '#recommend'; }

elseif($pCateSlug=='kids'):
  if(strstr($service, '七五三')||strstr($service, '早得')||strstr($service, 'おでかけ')||strstr($service, 'おまいり')||strstr($service, 'アクリル')):
    if(strstr($service, 'ベーシック')){
  	  $anchor = '#basic';
    }else{
    	$planUrl .= 'shichigosan/plan/';

	if(strstr($service, '早得')){ $anchor = '#c01'; }
	elseif(strstr($service, 'おでかけ')){ $anchor = '#c02'; }
	elseif(strstr($service, 'フル')){ $anchor = '#c04'; }
	elseif(strstr($service, 'おまいり')){ $anchor = '#c03'; }
	elseif(strstr($service, 'アクリル')){ $anchor = '#c05'; }
	else{ $anchor = '#recommend'; }
    }


  elseif(strstr($service, '宮参')||strstr($service, 'エンジェル')||strstr($service, 'スマイル')):
    $planUrl .= 'omiyamairi/';
    if(strstr($service, 'ベーシック')){ $anchor = '#basic'; }
    elseif(strstr($service, 'エンジェル')){ $anchor = '#c01'; }
    elseif(strstr($service, 'スマイル')){ $anchor = '#c02'; }
    else{ $anchor = '#recommend'; }

  elseif(strstr($service, '1歳')||strstr($service, '一歳')||strstr($service, 'よちよち')||strstr($service, 'すくすく')):
    $planUrl .= 'first_anniversary/';
    if(strstr($service, 'ベーシック')){ $anchor = '#basic'; }
    elseif(strstr($service, 'よちよち')){ $anchor = '#c01'; }
    elseif(strstr($service, 'すくすく')){ $anchor = '#c02'; }
    else{ $anchor = '#recommend'; }

  elseif(strstr($service, '成人')||strstr($service, '十三')):
    $planUrl .= 'tenth_anniversary/';
    if(strstr($service, '成人')){ $anchor = '#c01'; }
    elseif(strstr($service, '十三')||strstr($service, '13')||strstr($service, '１３')){ $anchor = '#c02'; }
    else{ $anchor = '#recommend'; }

  elseif(strstr($service, '入園')||strstr($service, '入学')||strstr($service, '卒園')||strstr($service, '卒業')||strstr($service, '袴')):
    $planUrl .= 'ceremony/';
    if(strstr($service, 'ベーシック')){ $anchor = '#basic'; }
    elseif(strstr($service, 'おでかけ')){ $anchor = '#c01'; }
    else{ $anchor = '#recommend'; }

  elseif(strstr($service, '誕生')):
    $planUrl .= 'birthday/';
    $anchor = '#basic';
  endif;
endif;

$planUrl .= $anchor;
//echo $planUrl;

if($pCateSlug!='other'):
?>
	<div class="utilizePlan">
		<a href="<?php echo $planUrl ?>" title="石川県金沢市の写真館「フォトスタジオ キノシタ」で、ご利用いただいたプランへ！"><img  width="504" height="84" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」で、ご利用いただいたプランへ！" src="<?php echo get_bloginfo('template_directory') ?>/images/voice/btn_01.gif" /></a>	
	</div>
<?php endif; ?>
</div><!-- #primary -->


<?php endwhile; else: ?>
    <p>投稿がありません</p>
<?php endif; ?>

<?php get_sidebar(); ?>

		</div><!-- #main -->

	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<a href="<?php bloginfo('url'); ?>/voice/" title="フォトスタジオ キノシタのお客様の声">お客様の声</a>&nbsp;＞&nbsp;<?php echo get_post_meta( $post->ID , 'お住まいのエリア' , ture ); ?>　<?php echo esc_attr( $post->post_title ); ?> 様
	</div><!-- End: bcList -->

<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>
