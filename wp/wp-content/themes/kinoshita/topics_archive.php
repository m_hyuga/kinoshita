<?php 
	/* Template Name: トピックス */
	wp_enqueue_style("topics_campaign_css", get_bloginfo('template_directory').'/css/topics_campaign.css' );
	$pageColumn = 2;
	$pageName = 'topics';
	get_header();
?>
	<!-- Start: bcList -->
	<div class="bcList Column2">
		■ 現在位置 : <a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		トピックス
	</div><!-- End: bcList -->

<div id="main" class="<?php echo $pageName ?> wrapper Column2">

  <div id="primary">
  
	<h3 title="フォトスタジオ キノシタのトピックス"><span>トピックス</span></h3>

<?php $topics = new WP_Query(array(
'posts_per_page' => 5,
'orderby' => 'date',
'order' => 'DESC'
));
if ($topics->have_posts()) :
	while($topics->have_posts()) : $topics->the_post(); ?>

	<!-- Start: Post -->
	<div class="post section">
	<dl>
	<dt><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></dt>
			
	<dd class="entry clearfix">
		<p>
<?php
$content = get_post_meta($post->ID,'リード文',true)." ";
$content .= strip_tags($post-> post_content);
$content = mb_substr($content, 0, 200);
echo $content;
?>
	<span class="detail"> <a href="<?php the_permalink(); ?>" title="フォトスタジオ キノシタのトピックス｜続きを読む...">続きを読む...</a></span>
	<p class="date"><?php the_time('Y年n月j日'); ?></p>
	</dl> 

    </div>
    <!-- End: Post -->

<?php endwhile; ?>
<?php endif; ?>

    <div class="more"><a href="<?php bloginfo('url'); ?>/news/category/topics/" title="フォトスタジオ キノシタの過去のトピックス">過去のトピックス</a></div>
 
<?php get_template_part( 'primary_footer' ); ?>

  </div>

<?php get_sidebar(); ?>
		

</div>
	<!-- Start: bcList -->
	<div class="bcList Column2">
		■ 現在位置 : <a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		トピックス
	</div><!-- End: bcList -->

<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>