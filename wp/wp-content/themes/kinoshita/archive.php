<?php	//topics月別・全件リスト（カテゴリー使用で出す）用テンプレート
	wp_enqueue_style("topics_campaign_css", http://www.studio-kinoshita.com/css/topics_campaign.css' );
	$offset = '0';
	if( $_REQUEST['past']){ $offset = '5'; }
	$pageName = 'topics';
get_header();?>

<?php if(is_month()): 
		$bc = get_the_time('Y年m月');
elseif(is_year()):
		$bc = get_the_time('Y年');
elseif(is_day()):
		$bc = get_the_time('Y年m月d日');
endif; ?>

	<!-- Start: bcList -->
	<div class="bcList">
		<a href="http://www.studio-kinoshita.com/" title="石川県金沢市の写真館スタジオキノシタ｜トップ">トップ</a>&nbsp;＞&nbsp;
		<a href="http://www.studio-kinoshita.com/news/" title="石川県金沢市の写真館スタジオキノシタ｜トピックス">トピックス</a>
<?php if(is_date()): ?>
		&nbsp;＞&nbsp;
		<?php echo $bc; ?>
<?php elseif(is_category()): ?>
		&nbsp;＞&nbsp;
		トピックス一覧
<?php endif; ?>
	</div><!-- End: bcList -->

<div id="main" class="wrapper topics Column2">

  <div id="primary">
  
    <h3><span>トピックス</span></h3>	


  <?php if ( is_category() ):
    $paged = get_query_var('paged'); //現在のページ番号取得
    $wp_query = new WP_Query(); // WP_Queryオブジェクト生成
    $param = array( //パラメータ。
        'paged' => $paged, //常に現在のページ番号を渡す
	'offset' => $offset,
        //'posts_per_page' => '1', //テスト
        'posts_per_page' => '10', //（整数）- 1ページに表示する記事数。-1 ならすべての投稿を取得。
        'post_status' => 'publish', //取得するステータスを指定：publish（公開済み）
	'orderby' => 'date',
        'order' => 'DESC' //降順。大きい値から小さい値の順。
    );
    $wp_query ->query($param);  // クエリにパラメータを渡す
else:

endif; ?>
<?php if($wp_query ->have_posts()): ?>
    <?php while ($wp_query ->have_posts()) : $wp_query ->the_post(); ?>

	<!-- Start: Post -->
	<div class="post section">
	<dl>
	<dt><a href="http://www.studio-kinoshita.com/"><?php the_title(); ?></a></dt>
			
	<dd class="entry clearfix">
		<p>
<?php
$content = get_post_meta($post->ID,'リード文',true)." ";
$content .= strip_tags($post-> post_content);
$content = mb_substr($content, 0, 200);
echo $content;
?>
	<span class="detail"> <a href="http://www.studio-kinoshita.com/" title="石川県金沢市の写真館スタジオキノシタ｜続きを読む...">続きを読む...</a></span>
	<p class="date"><?php the_time('Y年n月j日'); ?></p>
	</dl> 

    </div>
    <!-- End: Post -->
  <?php endwhile; ?>		
	<div class="navigation clearfix">
<p class="next"><?php next_posts_link('&nbsp;次のページ'); ?></p>
<p class="previous"><?php previous_posts_link('前のページ&nbsp;'); ?></p>
	</div>
  <?php endif; ?>
<?php wp_reset_query(); ?>

    <div class="more"><a href="http://www.studio-kinoshita.com/news/" title="石川県金沢市の写真館スタジオキノシタ｜最近のトピックス">最近のトピックス</a></div>

<?php get_template_part( 'primary_footer' ); ?>

</div>

<?php get_sidebar(); ?>
		

</div>

<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>