<?php
wp_enqueue_style("democss", get_bloginfo('template_directory').'/css/gallery/demo.css' );
wp_enqueue_style("stapelcss", get_bloginfo('template_directory').'/css/gallery/stapel.css' );
wp_enqueue_style("customcss", get_bloginfo('template_directory').'/css/gallery/custom.css' );
wp_enqueue_style("lightboxcss", get_bloginfo('template_directory').'/css/gallery/jquery.lightbox-0.5.css' );

wp_enqueue_script("stapeljs",get_bloginfo('template_directory').'/js/gallery/jquery.stapel.js', false, false, true );
wp_enqueue_script("mygalleryjs",get_bloginfo('template_directory').'/js/gallery/jquery.mygallery.js', false, false, true );
wp_enqueue_script("modernizr",get_bloginfo('template_directory').'/js/gallery/modernizr.custom.63321.js' );
wp_enqueue_script("lightboxjs",get_bloginfo('template_directory').'/js/gallery/jquery.lightbox-0.5.js' );

	$base_dir = '/home/sites/heteml/users/k/i/n/kino3/web/wp/wp-content/themes/kinoshita/images/gallery/';

?>

<?php 
	/* Template Name: フォトギャラリー */
	get_header();
?>

	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<?php the_title(); ?>
	</div><!-- End: bcList -->

	<div id="main" class="wrapper gallery Column1">

		<h3 title="フォトギャラリー">フォトギャラリー</h3>
			<div class="topbar">
				<span id="close" class="back">&larr;</span>
				<h4 id="name"></h4>
			</div>
					
<ul id="tp-grid" class="tp-grid">

				<!-- お宮参り・百日祝い -->
<?php $num = count(glob($base_dir."1/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li data-pile="お子様（赤ちゃん）のお宮参り・お食い初め（百日祝い）の写真">
					<a href="<?php echo get_bloginfo('template_directory') ?>/images/gallery/1/photo<?php echo $i; ?>.jpg" rel="lightbox[01]" title="フォトギャラリー｜お宮参り・百日祝い写真">
						<img src="<?php echo get_bloginfo('template_directory') ?>/images/gallery/1/photo<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>

				<!-- 七五三 -->
<?php $num = count(glob($base_dir."2/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li data-pile="お子様の七五三の写真">
					<a href="<?php echo get_bloginfo('template_directory') ?>/images/gallery/2/photo<?php echo $i; ?>.jpg" rel="lightbox[02]" title="フォトギャラリー｜七五三写真">
						<img src="<?php echo get_bloginfo('template_directory') ?>/images/gallery/2/photo<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>

				<!-- 1歳記念 -->
<?php $num = count(glob($base_dir."3/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li data-pile="お子様の初誕生・1歳記念・初節句の写真">
					<a href="<?php echo get_bloginfo('template_directory') ?>/images/gallery/3/photo<?php echo $i; ?>.jpg" rel="lightbox[03]" title="フォトギャラリー｜1歳記念写真">
						<img src="<?php echo get_bloginfo('template_directory') ?>/images/gallery/3/photo<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>

				<!-- 1/2成人式・十三参り -->
<?php $num = count(glob($base_dir."4/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li data-pile="お子様の10歳記念（1/2成人式）、十三参りの写真">
					<a href="<?php echo get_bloginfo('template_directory') ?>/images/gallery/4/photo<?php echo $i; ?>.jpg" rel="lightbox[04]" title="フォトギャラリー｜1/2成人式・十三参り写真">
						<img src="<?php echo get_bloginfo('template_directory') ?>/images/gallery/4/photo<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>

				<!-- お誕生日 -->
<?php $num = count(glob($base_dir."5/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li data-pile="お子様のお誕生日・記念写真">
					<a href="<?php echo get_bloginfo('template_directory') ?>/images/gallery/5/photo<?php echo $i; ?>.jpg" rel="lightbox[05]" title="フォトギャラリー｜お誕生日写真">
						<img src="<?php echo get_bloginfo('template_directory') ?>/images/gallery/5/photo<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>

				<!-- 入園・卒園／入学・卒業 -->
<?php $num = count(glob($base_dir."6/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li data-pile="お子様の卒園・入園・卒業・入学の記念写真">
					<a href="<?php echo get_bloginfo('template_directory') ?>/images/gallery/6/photo<?php echo $i; ?>.jpg" rel="lightbox[06]" title="フォトギャラリー｜入園・卒園／入学・卒業写真">
						<img src="<?php echo get_bloginfo('template_directory') ?>/images/gallery/6/photo<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>

				<!-- 成人・卒業 -->
<?php $num = count(glob($base_dir."7/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li data-pile="成人式・卒業式写真｜新成人を祝福する「成人の日」の記念に♪">
					<a href="<?php echo get_bloginfo('template_directory') ?>/images/gallery/7/photo<?php echo $i; ?>.jpg" rel="lightbox[07]" title="フォトギャラリー｜成人式・卒業式写真">
						<img src="<?php echo get_bloginfo('template_directory') ?>/images/gallery/7/photo<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>

				<!-- ブライダル -->
<?php $num = count(glob($base_dir."8/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li data-pile="結婚式写真・ブライダルフォト｜最高に幸せなウェディング、メモリアル記念♪">
					<a href="<?php echo get_bloginfo('template_directory') ?>/images/gallery/8/photo<?php echo $i; ?>.jpg" rel="lightbox[08]" title="フォトギャラリー｜結婚式写真・ブライダルフォト">
						<img src="<?php echo get_bloginfo('template_directory') ?>/images/gallery/8/photo<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>

				<!-- 家族・記念日 -->
<?php $num = count(glob($base_dir."9/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li data-pile="家族写真・記念写真｜結婚記念日や敬老の日のお祝い、マタニティフォトなど。">
					<a href="<?php echo get_bloginfo('template_directory') ?>/images/gallery/9/photo<?php echo $i; ?>.jpg" rel="lightbox[09]" title="フォトギャラリー｜家族写真・記念写真">
						<img src="<?php echo get_bloginfo('template_directory') ?>/images/gallery/9/photo<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>

				<!-- ポートレート・肖像写真 -->
<?php $num = count(glob($base_dir."10/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li data-pile="ポートレート・肖像写真｜肖像・プロフィール・オーディションなど。">
					<a href="<?php echo get_bloginfo('template_directory') ?>/images/gallery/10/photo<?php echo $i; ?>.jpg" rel="lightbox[10]" title="フォトギャラリー｜ポートレート・肖像写真">
						<img src="<?php echo get_bloginfo('template_directory') ?>/images/gallery/10/photo<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>




			</ul>
	</div><!-- #main .wrapper -->

	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<?php the_title(); ?>
	</div><!-- End: bcList -->

<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>