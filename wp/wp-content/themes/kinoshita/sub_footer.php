<div id="subFooter">

<?php global $pageColumn, $pageName; ?>
<?php if($pageColumn == 1 && $pageName != "dress"): ?>
	<div class="solution clearfix">
		<dl>
			<dt><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/solution_ttl.gif" width="922" height="45" alt="石川県金沢市のフォトスタジオ キノシタ｜お困りですか？" title="石川県金沢市のフォトスタジオ キノシタ｜お困りですか？"></dt>
			<dd class="faq"><a href="<?php site_top_url(); ?>/faq/" title="石川県金沢市のフォトスタジオ キノシタ｜よくある質問"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/solution_bnr01.jpg" width="249" height="74" alt="石川県金沢市のフォトスタジオ キノシタ｜よくある質問" border="0"></a></dd>
			<dd class="contact"><a href="https://www.studio-kinoshita.com/contact/" title="石川県金沢市のフォトスタジオ キノシタ｜お問い合わせ"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/solution_bnr02.jpg" width="249" height="74" alt="石川県金沢市のフォトスタジオ キノシタ｜お問い合わせ" border="0"></a></dd>
			<dd class="voice"><a href="<?php site_top_url(); ?>/voice/" title="石川県金沢市のフォトスタジオ キノシタ｜お客様の声"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/solution_bnr03.jpg" width="249" height="74" alt="石川県金沢市のフォトスタジオ キノシタ｜お客様の声" border="0"></a></dd>
		</dl>
	</div>
<?php endif; ?>

	<div class="pagetop">
		<a href="#page" title="石川県金沢市のフォトスタジオ キノシタ｜ページTOPへ"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/pagetop.gif" alt="石川県金沢市のフォトスタジオ キノシタ｜ページTOPへ" width="114" height="42"></a>
	</div>    

	<div class="quickLink">
	    <div class="quickLinkinner clearfix">
		<dl>
			<dt><h3 class="ql01"><a href="<?php site_top_url(); ?>/kids/omiyamairi/" title="お子様（赤ちゃん）のお宮参り・お食い初め（百日祝い）の写真撮影">お宮参り・百日祝い写真</a></h3></dt>
			<dd class="txt">赤ちゃんの健やかな成長をご家族でお祝いし、写真に収めましょう。</dd>
			<dd class="more"><a href="<?php site_top_url(); ?>/kids/omiyamairi/" title="お子様のお宮参り・百日祝い写真">more&gt;&gt;</a></dd>
		</dl>
		<dl>
			<dt><h3 class="ql02"><a href="<?php site_top_url(); ?>/kids/shichigosan/" title="お子様の七五三の写真撮影・七五三の衣装レンタル・着付け">お子様の七五三写真</a></h3></dt>
			<dd class="txt">お子様の晴れ姿を写真に収めて思い出に残しましょう。</dd>
			<dd class="more"><a href="<?php site_top_url(); ?>/kids/shichigosan/" title="お子様の七五三写真">more&gt;&gt;</a></dd>
		</dl>
		<dl>
			<dt><h3 class="ql03"><a href="<?php site_top_url(); ?>/kids/ceremony/" title="お子様の卒園・入園・卒業・入学の記念写真・卒園ハカマの外出衣装レンタル">入園/卒園・入学/卒業写真</a></h3></dt>
			<dd class="txt">お子様の新たなステージの始まりです。晴れ晴れしい姿を写真に収めておきましょう。</dd>
			<dd class="more"><a href="<?php site_top_url(); ?>/kids/ceremony/" title="お子様の入園/卒園・入学/卒業写真">more&gt;&gt;</a></dd>
		</dl>
		<dl class="last">
			<dt><h3 class="ql04"><a href="<?php site_top_url(); ?>/growup/" title="成人式・卒業式写真｜新成人を祝福する「成人の日」の記念に♪">成人式・卒業式写真</a></h3></dt>
			<dd class="txt">今日から大人の仲間入り。親元から巣立つそんな良き日を写真に収めましょう。</dd>
			<dd class="more"><a href="<?php site_top_url(); ?>/growup/" title="成人式・卒業式写真">more&gt;&gt;</a></dd>
		</dl>
	    </div><!-- .quickLinkinner -->
	</div><!-- .quickLink -->


</div><!-- /#subFooter -->