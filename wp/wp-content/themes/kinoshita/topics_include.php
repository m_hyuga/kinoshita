<?php //トピックスTOPとトピックスアーカイブ（日付・カテゴリー）のときのみ表示
if( $topics != 'topics_single' || is_date() == true || $myCat == "topics" ):
//customでリンクの前後につけるhtmlタグが指定可能に
//afterでセパレーターを渡す after=|
$html = wp_get_archives('type=monthly&show_post_count=1&format=custom&after=|&echo=0');

//$htmlの最後につく|と半角スペースを削除
$html = substr($html, 0, -2);

//セパレーターで配列に分割
$arr = explode("|", $html);

//リストの初年度と最終年を取得
//そのままだと文字列の前後に半角スペースが入っているのでtrimで詰める
$firstyear = substr(trim(strip_tags( $arr[0] )), 0, 4);
$lastyear = substr(trim(strip_tags( $arr[ count($arr)-1 ] )), 0, 4);

$cnt = 0;
$i = 0;
$tbody = null;
for( $year=$firstyear;$year>=$lastyear;$year-- ){
	$tbody .= "	<dt class='acMenu'>{$year}年</dt>
	";
	$tbody .= "<dd class='list{$i}'><ul>
	";
	for( $month=12;$month>=1;$month-- ){
		if( preg_match( "/".$year."年".$month."月/u", $arr[$cnt] ) ){
			$tbody .= "	<li>".preg_replace("/\d{4}年/u", "", trim($arr[$cnt]))."</li>
	";
			$cnt++;
		}
	}
$tbody .= "</ul></dd>
";
$i++;
}
$tbody .= "</dl>
";
echo "<div class='topicsList archive'>
<dl>{$tbody}"; ?>
		<div class="more"><a href="<?php site_top_url(); ?>/news/category/topics/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」トピックス一覧"><img src="<?php echo get_bloginfo('template_directory') ?>/images/topics/side_more.gif" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」トピックス一覧" width="113" height="20"></a></div>
	</div>
<?php endif; //トピックスTOPとトピックスアーカイブ（日付・カテゴリー）のときのみ表示 ここまで ?>

<?php //トピックス詳細のときのみ表示
if( $topics == 'topics_single' ):

$month = date('n');
$currentyear = date('Y'); 
$year = $currentyear;
?>

	<div class="topicsList">
		<dl>
			<dt><?php echo $year; ?>年</dt>
<?php
$i = 0;
$loopcnt = 0;
while( $i < 2 ):

	//記事一個のときのループ抜け処理、2年分走査
	if($loopcnt >= 24 ){ break; }

	if($loopcnt != 0):
		if($month == 1):
			$month = 12;
			$year--;
    	else:
			$month--;
		endif;
	endif;

	$myQuery = new WP_Query(); // WP_Queryオブジェクト生成
    	$param = array( //パラメータ。
			'year' => $year,
			'monthnum'=> $month,
        	'posts_per_page' => '-1', //（整数）- 1ページに表示する記事数。-1 ならすべての投稿を取得。
			'post_status' => 'publish', //取得するステータスを指定：publish（公開済み）
        	'order' => 'DESC' //降順。
		);
    $myQuery->query($param);  // クエリにパラメータを渡す

	if($myQuery->have_posts()):

		if($loopcnt != 0 && $currentyear != $year): ?>
			<dt><?php echo $year; ?>年</dt>
		<?php endif; ?>
			<dd><p><?php echo $month; ?>月</p>
				<ul>

<?php while($myQuery->have_posts()) : $myQuery->the_post();

$days=14;
$today=date('U'); $entry=get_the_time('U');
$diff1=date('U',($today - $entry))/86400;
?>
        			<li<? if ($days > $diff1) { echo ' class="new"';} ?>><a href="<?php the_permalink(); ?>" title="トピックス<?php the_title(); ?>の詳細へ"><?php the_title(); ?></a></li>

<?php endwhile; ?>
			</ul>
		</dd>
<?php $i++; ?>     
<?php endif; ?>
<?php $loopcnt++; ?>  
<?php endwhile; ?>
	</dl>
		<div class="more"><a href="<?php site_top_url(); ?>/news/category/topics/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」トピックス一覧"><img src="<?php echo get_bloginfo('template_directory') ?>/images/topics/side_more.gif" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」トピックス一覧" width="113" height="20"></a></div>
	</div>	

    
<?php endif; //トピックス詳細のときのみ表示 ここまで ?>
