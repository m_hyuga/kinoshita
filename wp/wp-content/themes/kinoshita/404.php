<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 */
	$pageColumn = 1;
	$pageName = esc_attr( $post->post_name );
get_header(); ?>

	<div id="main" class="<?php echo $pageName; ?> Column1">

<?php the_post(); ?>

		<div class="entry post-<?php the_ID(); ?>">
			<div class="prepare">
				<img src="<?php echo get_bloginfo('template_directory') ?>/images/common/404.png" width="510" height="61" alt="リクエストされたページが見つかりませんでした。URLが間違っているか、ページが削除されています。" title="リクエストされたページが見つかりませんでした。URLが間違っているか、ページが削除されています。">
<br><br><br>
			</div>
		</div>		
	</div>



<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>