<?php global $pageName; ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo(stylesheet_url); ?>">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<?php wp_head(); ?>

<?php if($pageName=='dress'): ?>
	<script>
		$(function() {
			$('a[rel^=lightbox]').lightBox();
		} );
	</script>
<?php endif; ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>

<!--[if gt IE 6]>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/textshadow.js"></script>
	<style>
<?php if(is_front_page()): ?>
		#header .subNaviArea h2 {
			font-family:"ＭＳ Ｐ明朝", "MS PMincho", "ヒラギノ明朝 Pro W3", "Hiragino Mincho Pro", serif;}
		#header .subNaviArea {
			background: url(images/home/copy_bg.gif) center 25px rgba(250,242,229,0.01) no-repeat; 
		}
<?php endif; ?>
		#footer h2 {
			font-family:"ＭＳ Ｐ明朝", "MS PMincho", "ヒラギノ明朝 Pro W3", "Hiragino Mincho Pro", serif;
		}
	</style>
<![endif]-->
<!--[if lt IE 8]>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.pseudo.js"></script>
<![endif]-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33861131-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script src="http://static.sppage.jp/YJ_redirect.js" type="text/javascript"></script>
<script type="text/javascript">YJ_redirect("http://s.sppage.jp/site/studio-kinoshita");</script>
</head>

<body <?php body_class(); ?>>
<?php echo $kids; ?>
<div id="page" class="hfeed site <?php echo $kids; ?>">

	<div id="header">
		<div class="siteInfo clearfix">
			<div class="ttl">
				<h2>七五三・成人・卒業・ブライダル・家族・記念日・証明写真など写真撮影なら、石川県金沢市のスタジオキノシタにおまかせください。</h2>
<?php if(is_front_page()): ?>
				<h1><img src="<?php echo get_bloginfo('template_directory') ?>/images/home/logo.png" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」お子様のお宮参りも、入学式、ポートレート・肖像写真などの写真撮影も格安で承っております。" width="286" height="84" title="石川県金沢市の写真館「フォトスタジオ キノシタ」お子様のお宮参りも、入学式、ポートレート・肖像写真などの写真撮影も格安で承っております。"></h1>
<?php else: ?>
				<h1><a href="<?php site_top_url(); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」お子様のお宮参りも、入学式、ポートレート・肖像写真などの写真撮影も格安で承っております。"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/logo.gif" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」お子様のお宮参りも、入学式、ポートレート・肖像写真などの写真撮影も格安で承っております。" width="286" height="84"></a></h1>

<?php endif; ?>
			</div>
			<div class="address">
				<ol>
					<li><a href="https://www.studio-kinoshita.com/contact/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」｜お問い合わせ">お問い合わせ</a>｜</li><!--
					--><li><a href="<?php site_top_url(); ?>/feature/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」｜会社案内">会社案内</a></li>
				</ol>
				<ul>
					<li class="map"><a href="<?php bloginfo('url'); ?>/access/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」｜MAP（アクセス・地図 ）"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_map.gif" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜MAP（アクセス・地図 ）" width="40" height="40"></a></li>
					<li class="tel">076-244-4649</li>
					<li>石川県金沢市平和町 2-11-11</li>
				</ul>
			</div>
		</div><!-- /.siteInfo -->

<?php if(is_front_page()): ?>
<div id="kv" class="slider">
<!-- your slider container -->
  <!-- and so on -->
  <div class="fs_loader"><!-- shows a loading .gif while the slider is getting ready --></div>

  <div class="slide">
	<img src="<?php echo get_bloginfo('template_directory') ?>/images/home/kv01.jpg">
  </div>
  <div class="slide">
	<img src="<?php echo get_bloginfo('template_directory') ?>/images/home/kv02.jpg">
  </div>
  <div class="slide">
	<img src="<?php echo get_bloginfo('template_directory') ?>/images/home/kv03.jpg">
  </div>
  <div class="slide">
	<img src="<?php echo get_bloginfo('template_directory') ?>/images/home/kv04.jpg">
  </div>
  <div class="slide">
	<img src="<?php echo get_bloginfo('template_directory') ?>/images/home/kv05.jpg">
  </div>


</div>
		<div class="globalNavi">
			<ul class="clearfix">
				<li><h3><a href="kids/" class="gn01" title="お子様｜すくすくと成長される子供の節目・記念に☆"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_navi01.png" alt="お子様｜すくすくと成長される子供の節目・記念に☆"></a></h3></li><!--
				--><li><h3><a href="growup/" class="gn02" title="成人式・卒業式写真｜新成人を祝福する「成人の日」の記念に♪"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_navi02.png" alt="成人式・卒業式写真｜新成人を祝福する「成人の日」の記念に♪" width="149" height="106"></a></h3></li><!--
				--><li><h3><a href="bridal/" class="gn03" title="結婚式写真・ブライダルフォト｜最高に幸せなウェディング、メモリアル記念♪"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_navi03.png" alt="結婚式写真・ブライダルフォト｜最高に幸せなウェディング、メモリアル記念♪" width="142" height="106"></a></h3></li><!--
				--><li><h3><a href="family/" class="gn04" title="家族写真・記念写真｜結婚記念日や敬老の日のお祝い、マタニティフォトなど。"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_navi04.png" alt="家族写真・記念写真｜結婚記念日や敬老の日のお祝い、マタニティフォトなど。" width="154" height="106"></a></h3></li><!--
				--><li><h3><a href="portrait/" class="gn05" title="ポートレート・肖像写真｜肖像・プロフィール・オーディションなど。"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_navi05.png" alt="ポートレート・肖像写真｜肖像・プロフィール・オーディションなど。" width="147" height="106"></a></h3></li><!--
				--><li><h3><a href="idphoto/" class="gn06" title="証明写真｜第一印象に差をつける！"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_navi06.png" alt="証明写真｜第一印象に差をつける！" width="147" height="106"></a></h3></li>
			</ul>      
		</div><!-- /.globalNavi -->
		<div class="subNaviArea">
			<div class="base"><h2>素敵な思い出、大切な人との絆を1枚の写真にこめて </h2></div>
			<div id="kidsMenu">
				<ul class="clearfix">
					<li><a href="kids/shichigosan/" title="お子様の七五三の写真撮影・七五三の衣装レンタル・着付け"><img src="<?php echo get_template_directory_uri(); ?>/images/home/kids_navi01_off.png" alt="お子様の七五三写真"></a></li><!--
					--><li><a href="kids/omiyamairi/" title="お子様（赤ちゃん）のお宮参り・お食い初め（百日祝い）の写真撮影"><img src="<?php echo get_template_directory_uri(); ?>/images/home/kids_navi02_off.png" alt="お子様のお宮参り・百日祝い写真"></a></li><!--
					--><li><a href="kids/first_anniversary/" title="お子様の初誕生・1歳記念・初節句の写真撮影・１歳撮影用の貸衣装"><img src="<?php echo get_template_directory_uri(); ?>/images/home/kids_navi03_off.png" alt="お子様の初誕生・1歳記念・初節句の写真"></a></li><!--
					--><li><a href="kids/tenth_anniversary/" title="お子様の10歳記念（1/2成人式）、十三参りの写真撮影・10歳用の貸衣装"><img src="<?php echo get_template_directory_uri(); ?>/images/home/kids_navi04_off.png" alt="お子様の10歳記念（1/2成人式）、十三参りの写真"></a></li><!--
					--><li><a href="kids/ceremony/" title="お子様の卒園・入園・卒業・入学の記念写真・卒園ハカマの外出衣装レンタル"><img src="<?php echo get_template_directory_uri(); ?>/images/home/kids_navi05_off.png" alt="お子様の入園/卒園・入学/卒業写真"></a></li><!--
					--><li><a href="kids/birthday/" title="お子様のお誕生日・記念写真・大きめサイズの撮影用衣装レンタル"><img src="<?php echo get_template_directory_uri(); ?>/images/home/kids_navi06_off.png" alt="お子様のお誕生日・記念写真"></a></li>
				</ul>
			</div>
		</div><!-- /.subNaviArea -->
<?php else: ?>
		<div class="globalNavi clearfix">
			<ul class="clearfix">
				<li><h3><a href="<?php site_top_url(); ?>/kids/" class="gn01" title="お子様｜すくすくと成長される子供の節目・記念に☆"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_sub_navi01.png" alt="お子様｜すくすくと成長される子供の節目・記念に☆" width="103" height="79"></a></h3></li><!--
				--><li><h3><a href="<?php site_top_url(); ?>/growup/" class="gn02" title="成人式・卒業式写真｜新成人を祝福する「成人の日」の記念に♪"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_sub_navi02.png" alt="成人式・卒業式写真｜新成人を祝福する「成人の日」の記念に♪" width="103" height="79"></a></h3></li><!--
				--><li><h3><a href="<?php site_top_url(); ?>/bridal/" class="gn03" title="結婚式写真・ブライダルフォト｜最高に幸せなウェディング、メモリアル記念♪"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_sub_navi03.png" alt="結婚式写真・ブライダルフォト｜最高に幸せなウェディング、メモリアル記念♪" width="103" height="79"></a></h3></li><!--
				--><li><h3><a href="<?php site_top_url(); ?>/family/" class="gn04" title="家族写真・記念写真｜結婚記念日や敬老の日のお祝い、マタニティフォトなど。"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_sub_navi04.png" alt="家族写真・記念写真｜結婚記念日や敬老の日のお祝い、マタニティフォトなど。" width="103" height="79"></a></h3></li><!--
				--><li><h3><a href="<?php site_top_url(); ?>/portrait/" class="gn05" title="ポートレート・肖像写真｜肖像・プロフィール・オーディションなど。"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_sub_navi05.png" alt="ポートレート・肖像写真｜肖像・プロフィール・オーディションなど。" width="103" height="79"></a></h3></li><!--
				--><li><h3><a href="<?php site_top_url(); ?>/idphoto/" class="gn06" title="証明写真｜第一印象に差をつける！"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/header_sub_navi06.png" alt="証明写真｜第一印象に差をつける！" width="107" height="79"></a></h3></li>
			</ul>
		</div><!-- /.globalNavi -->
		<div id="kv">
  <?php if($pageName=='topics'): ?>
			<img src="<?php echo get_bloginfo('template_directory') ?>/images/<?php echo $pageName; ?>/kv.jpg" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜トピックス" width="960" height="203" title="石川県金沢市の写真館「フォトスタジオ キノシタ」のトピックスはこちら！">
  <?php elseif($pageName=='campaign'): ?>
			<img src="<?php echo get_bloginfo('template_directory') ?>/images/<?php echo $pageName; ?>/kv.jpg" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜キャンペーン" width="960" height="203" title="石川県金沢市の写真館「フォトスタジオ キノシタ」のお得なキャンペーンはこちら！">  <?php elseif($pageName=='voice'): ?>
			<img src="<?php echo get_bloginfo('template_directory') ?>/images/<?php echo $pageName; ?>/kv.jpg" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜お客様の声" width="960" height="203" title="石川県金沢市の写真館「フォトスタジオ キノシタ」のお客様の声はこちら！">
  <?php elseif($pageName=='price'): ?>
			<img src="<?php echo get_bloginfo('template_directory') ?>/images/<?php echo $pageName; ?>/kv.jpg" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜料金・商品" width="960" height="203" title="石川県金沢市の写真館「フォトスタジオ キノシタ」の料金・商品はこちら！">
  <?php elseif($pageName=='faq'): ?>
			<img src="<?php echo get_bloginfo('template_directory') ?>/images/<?php echo $pageName; ?>/kv.jpg" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜よくある質問" width="960" height="203" title="石川県金沢市の写真館「フォトスタジオ キノシタ」のよくある質問はこちら！">
  <?php elseif($pageName=='bridal'): ?>
			<img src="<?php echo get_bloginfo('template_directory') ?>/images/<?php echo $pageName; ?>/kv.jpg" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜結婚式写真・ブライダルフォト" width="960" height="350" title="結婚式写真・ブライダルフォト｜最高に幸せなウェディング、メモリアル記念♪">
  <?php elseif($pageName=='idphoto'): ?>
			<img src="<?php echo get_bloginfo('template_directory') ?>/images/<?php echo $pageName; ?>/kv.jpg" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜証明写真" width="960" height="350" title="証明写真｜第一印象に差をつける！">
  <?php elseif($pageName=='portrait'): ?>
			<img src="<?php echo get_bloginfo('template_directory') ?>/images/<?php echo $pageName; ?>/kv.jpg" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜ポートレート・肖像写真" width="960" height="354" title="ポートレート・肖像写真｜肖像・プロフィール・オーディションなど。">
  <?php elseif($pageName=='growup'): ?>
			<img src="<?php echo get_bloginfo('template_directory') ?>/images/<?php echo $pageName; ?>/kv.jpg" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜成人式・卒業式写真" width="960" height="350" title="成人式・卒業式写真｜新成人を祝福する「成人の日」の記念に♪">
  <?php elseif($pageName=='family'): ?>
			<img src="<?php echo get_bloginfo('template_directory') ?>/images/<?php echo $pageName; ?>/kv.jpg" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜家族写真・記念写真" width="960" height="350" title="家族写真・記念写真｜結婚記念日や敬老の日のお祝い、マタニティフォトなど。">
  <?php endif; ?>
		</div>
<?php endif; ?>

	</div><!-- #header -->
