<?php 
	/* Template Name: 準備中用 */
	wp_enqueue_style("common_1clm_css", get_bloginfo('template_directory').'/css/common_1column.css' );
	$pageColumn = 1;
	$pageName = esc_attr( $post->post_name );
	get_header();
?>

	<!-- Start: bcList -->
	<div class="bcList">
		<a href="http://www.studio-kinoshita.com/" title="石川県金沢市の写真館スタジオキノシタ｜トップ">トップ</a>&nbsp;＞&nbsp;
		お客様の声
	</div><!-- End: bcList -->

	<div id="main" class="<?php echo $pageName; ?> Column1">

<?php the_post(); ?>

		<div class="entry post-<?php the_ID(); ?>">
			<div class="prepare">
				<img src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/common/prepare.png" width="510" height="570" alt="もうすぐ公開！ ちょっと待ってね" title="もうすぐ公開！ ちょっと待ってね">
			</div>
		</div>		
	</div>



<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>