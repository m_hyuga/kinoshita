<?php 
	/* Template Name: 2カラム用 */
	$pageColumn = 2;
	$pageName = esc_attr( $post->post_name );
        $parents = get_page_uri($post->post_parent);
	if($pageName!=$parents){
		$child = $pageName.'_'.$parents;
		$pageName = $parents;
	}
	if(strpos(get_bloginfo('name'), 'お子様')!== false){
		$kids = 'kids';
		wp_enqueue_style("kids_css", get_bloginfo('template_directory').'/css/kids.css' );
	}
	if($pageName=='growup'||$pageName=='bridal'):
		wp_enqueue_style($pageName."_css", get_bloginfo('template_directory').'/css/'.$pageName.'.css' );
	elseif(strpos(get_bloginfo('name'), 'お子様')=== false):
		wp_enqueue_style("common_2clm_css", get_bloginfo('template_directory').'/css/common_2column.css' );
	endif;

	get_header();
?>

	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php site_top_url(); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;

<?php if( $kids ){
	echo '<a href="'.get_bloginfo('url').'/" title="お子様｜すくすくと成長される子供の節目・記念に☆">お子様</a>&nbsp;＞&nbsp;';}
      if( $child ){
	echo '<a href="'.get_bloginfo('url').'/'.$parents.'/">'.get_the_title($post->post_parent).'</a>&nbsp;＞&nbsp;';
} ?>
		<?php the_title(); ?>
	</div><!-- End: bcList -->

	<div id="main" class="<?php echo $pageName; if($child){echo ' '.$child;}?> wrapper Column2">
		<div id="primary" class="site-content">

<?php the_post(); ?>
<?php if($pageName!='growup' && $pageName!='portrait' && $pageName!='bridal' && $pageName !='family' && $pageName !='idphoto' && $kids !='kids' ): ?>
			<h3 title="<?php the_title(); ?>"><span><?php the_title(); ?></span></h3>
<?php endif; ?>
			<div class="entry post-<?php the_ID(); ?>">
				<?php the_content(); ?>
			</div>

			<div class="solution_2clm clearfix">
				<dl>
					<dt><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/solution_ttl_2clm.gif" width="630" height="45" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜お困りですか？" title="お困りですか？石川県金沢市の写真館「フォトスタジオ キノシタ」におまかせください。"></dt>
					<dd class="faq"><a href="<?php bloginfo('url'); ?>/faq/" title="よくある質問｜石川県金沢市の写真館「フォトスタジオ キノシタ」におまかせください。"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/solution_bnr01.jpg" width="249" height="74" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜よくある質問" border="0"></a></dd>
					<dd class="contact"><a href="https://www.studio-kinoshita.com/contact/" title="お問い合わせ｜石川県金沢市の写真館「フォトスタジオ キノシタ」におまかせください。"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/solution_bnr02.jpg" width="249" height="74" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜お問い合わせ" border="0"></a></dd>
				</dl>
			</div>

		</div><!-- #primary -->

<?php get_sidebar(); ?>

	
	</div><!-- #main .wrapper -->



	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php site_top_url(); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;

<?php if( $kids ){
	echo '<a href="'.get_bloginfo('url').'/" title="お子様｜すくすくと成長される子供の節目・記念に☆">お子様</a>&nbsp;＞&nbsp;';}
      if( $child ){
	echo '<a href="'.get_bloginfo('url').'/'.$parents.'/">'.get_the_title($post->post_parent).'</a>&nbsp;＞&nbsp;';
} ?>
		<?php the_title(); ?>
	</div><!-- End: bcList -->


<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>