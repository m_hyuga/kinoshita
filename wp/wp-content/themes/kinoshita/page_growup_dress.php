<?php 
	/* Template Name: 成人・卒業衣装 */
	/*wp_enqueue_style("common_1clm_css", get_bloginfo('template_directory').'/css/common_1column.css' );*/
wp_enqueue_style("lightboxcss", get_bloginfo('template_directory').'/css/gallery/jquery.lightbox-0.5.css' );
wp_enqueue_script("lightboxjs",get_bloginfo('template_directory').'/js/gallery/jquery.lightbox-0.5.js' );
	$pageColumn = 1;
	$pageName = dress;
	$base_dir = '/home/sites/heteml/users/k/i/n/kino3/web/images/';
	get_header();
?>

	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<a href="<?php bloginfo('url'); ?>/growup/" title="成人式・卒業式写真｜新成人を祝福する「成人の日」の記念に♪">成人式・卒業式写真</a>&nbsp;＞&nbsp;
		衣装
	</div><!-- End: bcList -->

	<div id="main" class="Column1 dress growupDress">
    
<?php the_post(); ?>

		<h3 title="成人式・卒業式の衣装">成人式・卒業式の衣装</h3>
		<p class="bCopy">スタジオキノシタでは、撮影時にご利用いただける衣装をたくさんご用意しております。</p>

			<ul id="tab" class="floatList clearfix">
				<li class="btn01"><a href="#dress" class="active" title="成人式・卒業式の衣装｜振袖"><img src="<?php echo get_bloginfo('template_directory') ?>/images/growup/dress/tab_01_off.jpg" width="214" height="42" alt="成人式・卒業式の衣装｜振袖"></a></li>
				<li class="btn02"><a href="#rental" title="成人式・卒業式の衣装｜振袖外出レンタル"><img src="<?php echo get_bloginfo('template_directory') ?>/images/growup/dress/tab_02_off.jpg" width="214" height="42" alt="成人式・卒業式の衣装｜振袖外出レンタル"></a></li>
			</ul> 

		<div class="entry post-<?php the_ID(); ?>" id="dress">


			<ol class="clearfix">
				<li><a href="#dress01" title="成人式・卒業式の衣装｜振袖 Mサイズ"><img src="<?php echo get_bloginfo('template_directory') ?>/images/growup/dress/icon_01.gif" width="134" height="30" alt="成人式・卒業式の衣装｜振袖 Mサイズ"></a></li>
				<li><a href="#dress02" title="成人式・卒業式の衣装｜振袖 Lサイズ"><img src="<?php echo get_bloginfo('template_directory') ?>/images/growup/dress/icon_02.gif" width="134" height="30" alt="成人式・卒業式の衣装｜振袖 Lサイズ掛"></a></li>
				<li><a href="#dress03" title="成人式・卒業式の衣装｜振袖 LLサイズ"><img src="<?php echo get_bloginfo('template_directory') ?>/images/growup/dress/icon_03.gif" width="134" height="30" alt="成人式・卒業式の衣装｜振袖 LLサイズ"></a></li>

			</ol>        

		    <div id="dress01" class="category">
			<h4><img src="<?php echo get_bloginfo('template_directory') ?>/images/growup/dress/sttl_01.gif" width="235" height="57" alt="成人式・卒業式の衣装｜振袖 Mサイズ" title="成人式・卒業式の衣装｜振袖 Mサイズ"></h4>
			<ul class="clearfix">

<?php $num = count(glob($base_dir."growup_dress/00/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
					<a href="/images/growup_dress/00/00_<?php echo $i; ?>.jpg" rel="lightbox[00]" title="成人式・卒業式の衣装｜振袖 Mサイズ">
						<img src="/images/growup_dress/00/00_<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>
			</ul>
		    </div><!-- /#dress01 -->

  

		    <div id="dress02" class="category">
			<h4><img src="<?php echo get_bloginfo('template_directory') ?>/images/growup/dress/sttl_02.gif" width="235" height="57" alt="成人式・卒業式の衣装｜振袖 Lサイズ" title="成人式・卒業式の衣装｜振袖 Lサイズ"></h4>
			<ul class="clearfix">

<?php $num = count(glob($base_dir."growup_dress/01/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
					<a href="/images/growup_dress/01/01_<?php echo $i; ?>.jpg" rel="lightbox[01]" title="成人式・卒業式の衣装｜振袖 Lサイズ">
						<img src="/images/growup_dress/01/01_<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>
			</ul>
		    </div><!-- /#dress02 -->

	<div class="pagetop">
		<a href="#page" title="成人式・卒業式の衣装｜ページTOPへ"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/pagetop.gif" alt="成人式・卒業式の衣装｜ページTOPへ" width="114" height="42"></a>
	</div>    

		    <div id="dress03" class="category">
			<h4><img src="<?php echo get_bloginfo('template_directory') ?>/images/growup/dress/sttl_03.gif" width="235" height="57" alt="成人式・卒業式の衣装｜振袖 LLサイズ" title="成人式・卒業式の衣装｜振袖 LLサイズ"></h4>
			<ul class="clearfix">

<?php $num = count(glob($base_dir."growup_dress/02/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
					<a href="/images/growup_dress/02/02_<?php echo $i; ?>.jpg" rel="lightbox[02]" title="成人式・卒業式の衣装｜振袖 LLサイズ">
						<img src="/images/growup_dress/02/02_<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>
			</ul>
		    </div><!-- /#dress03 -->

		</div><!-- /entry -->	

		<div class="entry" id="rental"><!-- rental-->	
			<?php the_content(); ?>
		</div><!-- /rental -->	
	</div>

<?php get_template_part( 'sub_footer' ); ?>


	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<a href="<?php bloginfo('url'); ?>/growup/" title="成人式・卒業式写真｜新成人を祝福する「成人の日」の記念に♪">成人式・卒業式写真</a>&nbsp;＞&nbsp;
		衣装
	</div><!-- End: bcList -->


<?php get_footer(); ?>

