// JavaScript Document

myBr = 0;
if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || (navigator.userAgent.indexOf('Android') > 0  && navigator.userAgent.indexOf('Mobile')  > 0)) {
	myBr = 1;	//スマホの場合myBr=1
}

$(function() {

	equalHeight($(".quickLink dd.txt"));

	//グロナビ↓お子様メニューのSHOW/HIDE-------------------
	kidsmenu = 0;
	km = $('.home #kidsMenu');
	$('.home .gn01').mouseover(function() {
		if(kidsmenu == 0){
			km.css({'height':'0','top':'-8px'}).show().stop().animate({'height':'108px','top':'0'}, 300);
		}
		kidsmenu = 1;
	});
	km.mouseleave(function() {
		kidsMenuHide();
	});
	$('.home .gn02, .home .gn03, .home .gn04, .home .gn05').mouseover(function() {
		kidsMenuHide();
	});
	//ここまで グロナビ↓お子様メニューのSHOW/HIDE----------

	//TOPのキービジュアルのセンター寄せ-------------------
	$(window).on('resize load', function(){
	    if(myBr == 0){
		var winW = $(window).width();
		var ml = (winW-1200)/2
		$('.home #kv').css({'margin-left':ml+'px'});
	    }else{
		$('.home #kv').css({'margin-left':'-100px'});
		}
		
		if(window.location.hash){	//#付きのURLで飛んだ時に少し上まで見せる
			var href= $("div"+window.location.hash);
			var target = $( href == "" ? 'html' : href);
			var position = (target.offset().top) -30;
			$("html, body").animate({scrollTop:position}, 0);
			return false;
		}
	});
	//ここまで TOPのキービジュアルのセンター寄せ----------

	//TOP お子様メニューのhover処理-------------------
	$('.home #kidsMenu a img, .kids #kidsMenu a img').hover(function(){
		$(this).attr('src', $(this).attr('src').replace('_off', '_on'));
		$(this).addClass('on');
		}, function(){
			$(this).attr('src', $(this).attr('src').replace('_on', '_off'));
			$(this).removeClass('on');
	});
	//ここまで TOP お子様メニューのhover処理----------

	//サイドメニューのhover処理-------------------
	$('#secondary a img').hover(function(){
		$(this).addClass('on');
		}, function(){
			$(this).removeClass('on');
	});
	//ここまで サイドメニューのhover処理----------

	//トピックスのサイドアコーディオンの処理-------------------
	$(".topicsList dt.acMenu").on("click", function() {
		$(this).next().slideToggle();
	});
	//ここまで　トピックスのサイドアコーディオンの処理---------


	//衣装タブの処理-------------------
	$(".dress #tab li a").on("click", function() {
		$(".entry").hide();
		$($(this).attr("href")).fadeToggle();
		$(".dress #tab li a").removeClass("active");
		$(this).toggleClass("active");
		return false;
	});
	//ここまで　衣装タブの処理-------------------


	//電話番号の処理-------------------
	if(myBr == 1){
		$(".address .tel").html('<a href="tel:0762444649">076-244-4649</a>');
		$(".contact strong").html('<a href="tel:0762444649">076-244-4649</a>');
		var telImg = $(".faq dd.faq img").attr('src');
//		$(".faq dd.faq").html('<p>お電話でのお問い合わせ</p><a href="tel:0762444649"><img src="'+telImg+'" width="284" height="104" alt="076 - 244 - 4649 平日:10:00~19:00" title="076 - 244 - 4649 平日:10:00~19:00" border="0"></a>');

	}
	//ここまで　電話番号の処理-------------------
	



} );


//boxの高さ揃え
function equalHeight(group) {
	tallest = 0;
	group.each(function() {
		thisHeight = $(this).height();
		if(thisHeight > tallest){
			tallest = thisHeight;
		}
	});
	group.height(tallest);
}
/*	boxの高さ揃え説明
       //.hogeの中にあるdivの高さを全て同じにする  
        equalHeight($("div.hoge > div"));  
        //.hoge-singleを付与された要素同士の高さを揃える  
        equalHeight($("div.hoge-single"));  
        //.hoge-Lと.hoge-Rの高さを揃える  
        equalHeight($("div.hoge-L,div.hoge-R"));  
*/

//グロナビ↓お子様メニューのHIDE-------------------
function kidsMenuHide(){
	if(kidsmenu == 1){
		km.stop().animate({'height':'0','top':'-8px'},{
			'duration': 200,
			'complete': function(){
				km.hide();
				kidsmenu = 0;
			}
		});
	}
}
