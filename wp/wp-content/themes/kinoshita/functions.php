<?php

/* =========================================================

	基本設定

========================================================= */
/* ネットワーク対応
--------------------------------------------------------- */
define('WP_ALLOW_MULTISITE', true);

/* URL
--------------------------------------------------------- */
function site_top_url() {
	$site_url = get_blogaddress_by_id(1);
	$site_url = substr( $site_url , 0 , strlen($site_url)-1 );
	echo $site_url;
}
function shortcode_site_top_url() {
	$site_url = get_blogaddress_by_id(1);
	$site_url = substr( $site_url , 0 , strlen($site_url)-1 );
	return $site_url;
}
add_shortcode('site_top_url', 'shortcode_site_top_url');

/* 管理バー非表示
--------------------------------------------------------- */
function my_function_admin_bar() {
	return false;
}
add_filter( 'show_admin_bar' , 'my_function_admin_bar');

/* pタグ除去
--------------------------------------------------------- */
remove_filter('the_excerpt', 'wpautop');
remove_filter('the_content', 'wpautop');

/* カレンダーの未来日付の記事を公開
--------------------------------------------------------- */
function forced_publish_future_post( $data, $postarr ) {
	if ( $data['post_status'] == 'future' && $data['post_type'] == 'daily' && $postarr['post_status'] == 'publish' ) {
		$data['post_status'] = 'publish';
	}
	return $data;
}
add_filter( 'wp_insert_post_data', 'forced_publish_future_post', 10, 2 );

/* style・scriptローダーのバージョン表記削除
--------------------------------------------------------- */
add_filter('style_loader_src', 'remove_version');
add_filter('script_loader_src', 'remove_version');

if (!function_exists('remove_version')) {
	function remove_version($src) {
		if (strpos($src, '?ver=')) $src = remove_query_arg('ver', $src);
		return $src;
	}
}

/* テンプレートフォルダのパスをショートコードに登録
--------------------------------------------------------- */
function shortcode_templateurl() {
    return get_bloginfo('template_url');
}
add_shortcode('template_url', 'shortcode_templateurl');
/*子サイト用*/
function shortcode_kidstemplateurl() {
    switch_to_blog(2);
    return get_bloginfo('stylesheet_directory');
    restore_current_blog();
}
add_shortcode('kids_template_url', 'shortcode_kidstemplateurl');


/* ショートコードをビジュアルエディタ内で置換
--------------------------------------------------------- */
class EditorPlugin {
    function EditorPlugin() {
        add_action('init', array(&$this, 'addplugin'));
    }
    function sink_hooks(){
        add_filter('mce_plugins', array(&$this, 'mce_plugins'));
    }
    function addplugin() {
       if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
         return;
       //リッチエディタの時だけ追加
       if ( get_user_option('rich_editing') == 'true') {
         add_filter("mce_external_plugins", array(&$this, 'mce_external_plugins'));
       }
    }
    // TinyMCE プラグインファイルを読み込む: editor_plugin.js
    function mce_external_plugins($plugin_array) {
       //プラグイン関数名＝ファイルの位置
       $plugin_array['ShortcodeConv'] = get_bloginfo('template_directory').'/js/editor_plugin.js';
       return $plugin_array;
    }
}
$myeditorplugin = new EditorPlugin();
add_action('init',array(&$myeditorplugin, 'EditorPlugin'));









/* 画像リサイズ
--------------------------------------------------------- */
//投稿（トピックス）用
add_image_size('topics_image',400, 9999);
function topics_image () {
	echo wp_get_attachment_image(post_custom('画像'),'topics_image');
}
//大バナー
add_image_size('bannerL',620, 9999);
function bannerL_image ($val) {
	echo wp_get_attachment_image(post_custom($val),'bannerL', false, array( 'alt'=> get_the_title( ),  'title'=> get_the_title( )) );
}

//お客様の声用
add_image_size('voice_eyechatch',201, 201);
function voice_eyechatch_image () {
	echo wp_get_attachment_image(post_custom('アイキャッチ'),'voice_eyechatch');
}
//お客様の声詳細用
add_image_size('voice_eyechatch_L',636, 636);
function voice_eyechatch_image2 () {
	echo wp_get_attachment_image(post_custom('アイキャッチ'),'voice_eyechatch_L');
}

//お客様の声詳細用（サムネイル）
add_image_size('voice_eyechatch_M',290, 290);
function voice_eyechatch_image3 ($val) {
	echo wp_get_attachment_image(post_custom($val),'voice_eyechatch_M');
}

function voice_eyechatch_size ($photo) {
global $post;
	$addClass = '';
	$image01 = get_post_meta($post->ID, $photo, true);
	$thumb = wp_get_attachment_image_src($image01, 'full');
	$thumbU = $thumb[0];
	$thumbW = $thumb[1];
	$thumbH = $thumb[2];

	if($thumbH > $thumbW){
		$addClass = 'oblong';
	}
	
	echo $addClass;

}

function voice_eyechatch_sizeW () {
global $post;
	$image01 = get_post_meta($post->ID, 'アイキャッチ', true);
	$thumb = wp_get_attachment_image_src($image01, 'full');
	$thumbU = $thumb[0];
	$thumbW = $thumb[1];
	$thumbH = $thumb[2];

	if($thumbW > 636){
		$thumbWidth  = 636;
	}else{$thumbWidth  = $thumbW;}

	$WidthTotal = $thumbWidth + 14; 
	
	echo $WidthTotal;

}





/* 記事の最初または最後を判別
--------------------------------------------------------- */
function isFirst($query){
	global $wp_query;
	if(!$query)$query=$wp_query;
    return ($query->current_post === 0);
}

function isLast($query){
	global $wp_query;
	if(!$query)$query=$wp_query;
    return ($query->current_post+1 === $query->post_count);
}



/* =========================================================

	カスタム投稿タイプ

========================================================= */
/* 営業カレンダー　イベント入力カスタム投稿
--------------------------------------------------------- */
// カスタム投稿タイプを作成
function daily_custom_post_type()
{
$labels = array(
'name' => _x('営業カレンダー', 'post type general name'),
'add_new' => _x('定休日等を追加', 'daily'),
'all_items' => __('営業情報一覧'),
'add_new_item' => __('新しい営業情報を追加'),
'edit_item' => __('営業情報を編集'),
'new_item' => __('新しい営業情報'),
'view_item' => __('投稿を表示'),
'search_items' => __('営業情報を探す'),
'not_found' => __('営業情報はありません'),
'not_found_in_trash' => __('ゴミ箱に営業情報はありません'),
'parent_item_colon' => ''
);
$args = array(
'labels' => $labels,
'public' => true,
'publicly_queryable' => true,
'show_ui' => true,
'query_var' => true,
'rewrite' => array(true, 'with_front' => false),
'capability_type' => 'post',
'hierarchical' => false,
'menu_position' => 5,
'has_archive' => true,
'supports' => array('title','editor'),
'taxonomies' => array('daily_category')
);
register_post_type('daily',$args);
// カスタムタクソノミーを作成
//カテゴリータイプ
$args = array(
'label' => 'カテゴリー',
'public' => true,
'show_ui' => true,
'hierarchical' => true
);
register_taxonomy('daily_category','daily',$args);
}
add_action('init', 'daily_custom_post_type');

/* キャンペーン　カスタム投稿
--------------------------------------------------------- */
// カスタム投稿タイプを作成
function campaign_custom_post_type()
{
$labels = array(
'name' => _x('キャンペーン', 'post type general name'),
'add_new' => _x('キャンペーンを追加', 'campaign'),
'all_items' => __('キャンペーン一覧'),
'add_new_item' => __('新しいキャンペーンを追加'),
'edit_item' => __('キャンペーンを編集'),
'new_item' => __('新しいキャンペーン'),
'view_item' => __('投稿を表示'),
'search_items' => __('キャンペーンを探す'),
'not_found' => __('キャンペーンはありません'),
'not_found_in_trash' => __('ゴミ箱にキャンペーンはありません'),
'parent_item_colon' => ''
);
$args = array(
'labels' => $labels,
'public' => true,
'publicly_queryable' => true,
'show_ui' => true,
'query_var' => true,
'rewrite' => array(true, 'with_front' => false),
'capability_type' => 'post',
'hierarchical' => false,
'menu_position' => 5,
'has_archive' => true,
'supports' => array('title','editor'),
'taxonomies' => array('campaign_category')
);
register_post_type('campaign',$args);
// カスタムタクソノミーを作成
//カテゴリータイプ
$args = array(
'label' => 'カテゴリー',
'public' => true,
'show_ui' => false,
'hierarchical' => true
);
register_taxonomy('campaign_category','campaign',$args);
}
add_action('init', 'campaign_custom_post_type');


/* お客様の声　カスタム投稿
--------------------------------------------------------- */
// カスタム投稿タイプを作成
function voice_custom_post_type()
{
$labels = array(
'name' => _x('お客様の声', 'post type general name'),
'add_new' => _x('お客様の声を追加', 'voice'),
'all_items' => __('お客様の声一覧'),
'add_new_item' => __('新しいお客様の声を追加'),
'edit_item' => __('お客様の声を編集'),
'new_item' => __('新しいお客様の声'),
'view_item' => __('投稿を表示'),
'search_items' => __('お客様の声を探す'),
'not_found' => __('お客様の声はありません'),
'not_found_in_trash' => __('ゴミ箱にお客様の声はありません'),
'parent_item_colon' => ''
);
$args = array(
'labels' => $labels,
'public' => true,
'publicly_queryable' => true,
'show_ui' => true,
'query_var' => true,
'rewrite' => array(true, 'with_front' => false),
'capability_type' => 'post',
'hierarchical' => false,
'menu_position' => 5,
'has_archive' => true,
'supports' => array('title','editor'),
'taxonomies' => array('voice_category')
);
register_post_type('voice',$args);
// カスタムタクソノミーを作成
//カテゴリータイプ
$args = array(
'label' => 'カテゴリー',
'public' => true,
'show_ui' => false,
'hierarchical' => true
);
register_taxonomy('voice_category','voice',$args);
}
add_action('init', 'voice_custom_post_type');

/* よくある質問　カスタム投稿
--------------------------------------------------------- */
// カスタム投稿タイプを作成
function faq_custom_post_type()
{
$labels = array(
'name' => _x('よくある質問', 'post type general name'),
'add_new' => _x('よくある質問を追加', 'faq'),
'all_items' => __('よくある質問一覧'),
'add_new_item' => __('新しいよくある質問を追加'),
'edit_item' => __('よくある質問を編集'),
'new_item' => __('新しいよくある質問'),
'view_item' => __('投稿を表示'),
'search_items' => __('よくある質問を探す'),
'not_found' => __('よくある質問はありません'),
'not_found_in_trash' => __('ゴミ箱によくある質問はありません'),
'parent_item_colon' => ''
);
$args = array(
'labels' => $labels,
'public' => true,
'publicly_queryable' => true,
'show_ui' => true,
'query_var' => true,
'rewrite' => array(true, 'with_front' => false),
'capability_type' => 'post',
'hierarchical' => false,
'menu_position' => 5,
'has_archive' => true,
'supports' => array('title','editor'),
'taxonomies' => array('faq_category')
);
register_post_type('faq',$args);
// カスタムタクソノミーを作成
//カテゴリータイプ
$args = array(
'label' => 'カテゴリー',
'public' => true,
'show_ui' => false,
'hierarchical' => true
);
register_taxonomy('faq_category','faq',$args);
}
add_action('init', 'faq_custom_post_type');
//flush_rewrite_rules( true );		//ルールの初期化、一度読めれば以後不要










/* =========================================================

	ウィジェット

========================================================= */
/* 営業カレンダー
--------------------------------------------------------- */
function open_get_calendar($initial = true, $echo = true) {
	global $wpdb, $m, $monthnum, $year, $wp_locale, $posts, $cat;

	$cache = array();
	$key = md5( $m . $monthnum . $year );
	if ( $cache = wp_cache_get( 'get_calendar', 'calendar' ) ) {
		if ( is_array($cache) && isset( $cache[ $key ] ) ) {
			if ( $echo ) {
				echo apply_filters( 'get_calendar',  $cache[$key] );
				return;
			} else {
				return apply_filters( 'get_calendar',  $cache[$key] );
			}
		}
	}

	if ( !is_array($cache) )
		$cache = array();

	// Quick check. If we have no posts at all, abort!
	if ( !$posts ) {
		$gotsome = $wpdb->get_var("SELECT 1 as test FROM $wpdb->posts WHERE post_type = 'daily' AND post_status = 'publish' LIMIT 1");
		if ( !$gotsome ) {
			$cache[ $key ] = '';
			wp_cache_set( 'get_calendar', $cache, 'calendar' );
			return;
		}
	}

	if ( isset($_GET['w']) )
		$w = ''.intval($_GET['w']);

	// week_begins = 0 stands for Sunday
	//$week_begins = intval(get_option('start_of_week'));
	$week_begins = 0;	//日曜はじまりにする
	
	// Let's figure out when we are
	if ( isset($_REQUEST['adyear']) && isset($_REQUEST['monthsnum']) ) {
		$thisyear = $_REQUEST['adyear'];
		$thismonth = $_REQUEST['monthsnum'];
	} else {
		if ( !empty($monthnum) && !empty($year) ) {
			$thismonth = ''.zeroise(intval($monthnum), 2);
			$thisyear = ''.intval($year);
		} elseif ( !empty($w) ) {
			// We need to get the month from MySQL
			$thisyear = ''.intval(substr($m, 0, 4));
			$d = (($w - 1) * 7) + 6; //it seems MySQL's weeks disagree with PHP's
			$thismonth = $wpdb->get_var("SELECT DATE_FORMAT((DATE_ADD('${thisyear}0101', INTERVAL $d DAY) ), '%m')");
		} elseif ( !empty($m) ) {
			$thisyear = ''.intval(substr($m, 0, 4));
			if ( strlen($m) < 6 )
					$thismonth = '01';
			else
					$thismonth = ''.zeroise(intval(substr($m, 4, 2)), 2);
		} else {
			$thisyear = gmdate('Y', current_time('timestamp'));
			$thismonth = gmdate('m', current_time('timestamp'));
		}
	}

	$unixmonth = mktime(0, 0 , 0, $thismonth, 1, $thisyear);
	
	// Get the next and previous month and year with at least one post
	$previous = $wpdb->get_row("SELECT DISTINCT MONTH(post_date) AS month, YEAR(post_date) AS year
		FROM $wpdb->posts
		WHERE post_date < '$thisyear-$thismonth-01'
		AND post_type = 'daily' AND post_status = 'publish'
			ORDER BY post_date DESC
			LIMIT 1");
	$next = $wpdb->get_row("SELECT DISTINCT MONTH(post_date) AS month, YEAR(post_date) AS year
		FROM $wpdb->posts
		WHERE post_date >	'$thisyear-$thismonth-01'
		AND MONTH( post_date ) != MONTH( '$thisyear-$thismonth-01' )
		AND post_type = 'daily' AND post_status = 'publish'
			ORDER	BY post_date ASC
			LIMIT 1");

	/* translators: Calendar caption: 1: month name, 2: 4-digit year */
	$engM = array('','-Jun-','-Feb-','-Mar-','-Apr-','-May-','-Jun-','-Jul-','-Aug-','-Sep-','-Oct-','-No-v','-Dec-');
	if( $thisyear == date('Y', $unixmonth)){
		$calendar_caption = _x('%1$s %3$s', 'calendar caption');		//今年の場合年を表示しない
		$clndr_cap = sprintf($calendar_caption, $wp_locale->get_month($thismonth), date('Y年', $unixmonth), $engM[intval($thismonth)] );
	} else {
		$calendar_caption = _x('%2$s %1$s %3$s', 'calendar caption');		//今年じゃない場合年を表示する
		$clndr_cap = sprintf($calendar_caption, $wp_locale->get_month($thismonth), date('Y年', $unixmonth), $engM[intval($thismonth)]);
	}
	$calendar_output = '<table>
	<caption>' . $clndr_cap . '</caption>
	<thead>
	<tr>';

	$myweek = array();

	for ( $wdcount=0; $wdcount<=6; $wdcount++ ) {
		$myweek[] = $wp_locale->get_weekday(($wdcount+$week_begins)%7);
	}

$myWeek = 1;
	foreach ( $myweek as $wd ) {
		$day_name = (true == $initial) ? $wp_locale->get_weekday_initial($wd) : $wp_locale->get_weekday_abbrev($wd);
		$wd = esc_attr($wd);
		$calendar_output .= "\n\t\t<th scope=\"col\" title=\"$wd\" class=\"w$myWeek\">$day_name</th>";
		$myWeek++;
	}
		$calendar_output .= '

	</tr>
	</thead>
	<tbody>
	<tr>';
	
	// Get days with posts
		$dayswithposts = $wpdb->get_results("SELECT DISTINCT DAYOFMONTH(post_date)
			FROM $wpdb->posts WHERE MONTH(post_date) = ‘$thismonth’
			AND YEAR(post_date) = ‘$thisyear’
			AND post_type = ‘daily’ AND post_status = ‘publish’ ", ARRAY_N);
			
	if ( $dayswithposts ) {
		foreach ( (array) $dayswithposts as $daywith ) {
			$daywithpost[] = $daywith[0];
		}
	} else {

		$daywithpost = array();
	}

	if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false || stripos($_SERVER['HTTP_USER_AGENT'], 'camino') !== false || stripos($_SERVER['HTTP_USER_AGENT'], 'safari') !== false)
		$ak_title_separator = "\n";
	else
		$ak_title_separator = ', ';

	$ak_titles_for_day = array();
	
	// Remove the limit of day's posts
/*	$ak_post_titles = $wpdb->get_results("SELECT ID, post_title, guid, DAYOFMONTH(post_date) as dom "
		."FROM $wpdb->posts "
		."WHERE YEAR(post_date) = '$thisyear' "
		."AND MONTH(post_date) = '$thismonth' "
		."AND post_type = 'daily' AND post_status = 'publish'"
	);
*/	

	$ak_post_titles = $wpdb->get_results("
		SELECT *, DAYOFMONTH(post_date) as dom
		FROM  {$wpdb->posts} posts LEFT JOIN {$wpdb->term_relationships} t_rel  ON posts.ID = t_rel.object_id
    	LEFT JOIN {$wpdb->term_taxonomy} t_term_tx ON t_rel.term_taxonomy_id = t_term_tx.term_taxonomy_id 
    	LEFT JOIN {$wpdb->terms} t_terms ON t_term_tx.term_id = t_terms.term_id 
		WHERE t_term_tx.taxonomy = 'daily_category' 
		AND YEAR(post_date) = '$thisyear' 
		AND MONTH(post_date) = '$thismonth'
		AND post_type = 'daily' AND post_status = 'publish'
	");

	// $custom_postDataArr = days , title , guide , termID , name(カテゴリ名)	書き変え後
	foreach ($ak_post_titles as $value) {
		$custom_postDataArr[] = array($value->dom, $value->post_title, get_permalink($value->ID), $value->term_id, $value->name );
		if( !$custom_catArr[$value->term_id] ){
			$custom_catArr[$value->term_id] = $value->name;
		}
	}

	if ( $ak_post_titles ) {
		foreach ( (array) $ak_post_titles as $ak_post_title ) {

				$post_title = esc_attr( apply_filters( 'the_title', $ak_post_title->post_title, $ak_post_title->ID ) );

				if ( empty($ak_titles_for_day['day_'.$ak_post_title->dom]) )
					$ak_titles_for_day['day_'.$ak_post_title->dom] = '';
				if ( empty($ak_titles_for_day["$ak_post_title->dom"]) ) // first one
					$ak_titles_for_day["$ak_post_title->dom"] = $post_title;
				else
					$ak_titles_for_day["$ak_post_title->dom"] .= $ak_title_separator . $post_title;
		}
	}

	// See how much we should pad in the beginning
	$pad = calendar_week_mod(date('w', $unixmonth)-$week_begins);
	if ( 0 != $pad ) {
		$i = 0;
		while(true) {
			$calendar_output .= "\n\t\t".'<td>&nbsp;</td>';
			$i++;
			if( $i == $pad ) break;
		}
	}

	$daysinmonth = intval(date('t', $unixmonth));
	for ( $day = 1; $day <= $daysinmonth; ++$day ) {
		if ( isset($newrow) && $newrow )
			$calendar_output .= "\n\t</tr>\n\t<tr>\n\t\t";
		$newrow = false;
		
		if ( $day == gmdate('j', current_time('timestamp')) && $thismonth == gmdate('m', current_time('timestamp')) && $thisyear == gmdate('Y', current_time('timestamp')) ){
			$calendar_output .= '<td class="today ';
		}else{
			$calendar_output .= '<td  class="';
		}
		   for($i=0; $i < count($custom_postDataArr);$i++){
			   if($custom_postDataArr[$i][0] == $day){
				$calendar_output .= 
							"cat". $custom_postDataArr[$i][3];
			   }
		   }
				$calendar_output .= ' " title="';
				$loopcnt = 0;
		   for($i=0; $i < count($custom_postDataArr);$i++){
			   if($custom_postDataArr[$i][0] == $day){
				$calendar_output .= 
							 $custom_catArr[$custom_postDataArr[$i][3]];	//タクソノミーID（$custom_postDataArr[$i][3]）からタクソノミー名を表示
					if($loopcnt > 0){
						$calendar_output .= "・";
					}
			   		++$loopcnt;
			   }
		   }
/*				$calendar_output .= "\"><dl>
		<dt>".$day."</dt>
		<dd>&nbsp;</dd>\n</dl>\n</td>\n";
*/
				$calendar_output .= "\"><dl>\n
				<dd>&nbsp;</dd>\n<dt>".$day."</dt>\n
				</dl>\n</td>\n";

		if ( 6 == calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins) )
			$newrow = true;
	}

	$pad = 7 - calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins);
	if ( $pad != 0 && $pad != 7 ) {
		$i = 0;
		while(true) {
			$calendar_output .= "\n\t\t".'<td>&nbsp;</td>';
			$i++;
			if( $i == $pad ) break;
		}
	}
		//$calendar_output .= "\n\t\t".'<td class="pad" colspan="'. esc_attr($pad) .'">&nbsp;</td>';

	$calendar_output .= "\n\t</tr>\n\t</tbody>\n</table>\n";
	
	$calendar_output .= '<ul class="calendarNavi">';	
	if ( $previous ) {
	    if( $previous->month <= $thismonth || $previous->year < $thisyear ){
		$calendar_output .= "\n\t\t".'<li class="prev"><a href="?adyear=' .$previous->year . '&monthsnum=' .$previous->month. '" title="' . esc_attr( sprintf(__('View posts for %1$s %2$s'), $wp_locale->get_month($previous->month), date('Y', mktime(0, 0 , 0, $previous->month, 1, $previous->year)))) . '">&laquo; ' . $wp_locale->get_month_abbrev($wp_locale->get_month($previous->month)) . '</a></li>';
	} else {
		$calendar_output .= "\n\t\t".'<li class="prev pad">&nbsp;</li>';
	    }
	}
	if ( $next ) {
		$calendar_output .= "\n\t\t".'<li class="next"><a href="?adyear=' .$next->year . '&monthsnum=' .$next->month. '" title="' . esc_attr( sprintf(__('View posts for %1$s %2$s'), $wp_locale->get_month($next->month), date('Y', mktime(0, 0 , 0, $next->month, 1, $next->year))) ) . '">' . $wp_locale->get_month_abbrev($wp_locale->get_month($next->month)) . ' &raquo;</a></li>';
	} else {
		$calendar_output .= "\n\t\t".'<li class="next pad">&nbsp;</li>';
	}
		$calendar_output .= '
	</ul>';


	$cache[ $key ] = $calendar_output;
	wp_cache_set( 'get_calendar', $cache, 'calendar' );


//営業スケジュールの下に出すカテゴリー説明
ksort($custom_catArr);
		$calendar_output .= "</div>\n<div class=\"colorTip\">\n<ul>\n";
		foreach($custom_catArr as $key => $value){
			$calendar_output .=  '<li><span class="cat'.$key.'">■</span>'.$value.'</li>';
		}
		$calendar_output .= "\n</ul>\n";


	if ( $echo )
		echo apply_filters( 'get_calendar',  $calendar_output );
	else
		return apply_filters( 'get_calendar',  $calendar_output );

}
/*---------------------------------------------------------
 営業カレンダーここまで */





?>