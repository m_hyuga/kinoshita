<?php
	wp_enqueue_style("topics_campaign_css", get_bloginfo('template_directory').'/css/topics_campaign.css' );
	$pageName = 'campaign';
	$campaign= 'campaign_single';
get_header(); ?>

	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<a href="<?php bloginfo('url'); ?>/campaign/" title="スタジオ キノシタのキャンペーン">キャンペーン</a>&nbsp;＞&nbsp;<?php echo esc_attr( $post->post_title ); ?>
	</div><!-- End: bcList -->

<div id="main" class="wrapper Column2 campaign">
  <div id="primary">

	<h3 title="スタジオ キノシタのキャンペーン"><span>キャンペーン</span></h3>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div class="post">
        <dl>
        <dt><?php the_title(); ?>
<?php 
if (strtotime(date('Y/m/d')) >= strtotime(get_post_meta($post->ID,'終了日',true)))
{echo '<img src="'.get_bloginfo('template_directory').'/images/campaign/end.gif" alt="終了" title="終了" />';}
?>
</dt>
        <dd class="date"><?php the_time('Y年m月d日'); ?></dd>
	<dd><p class="read"><?php echo get_the_content(); ?></p>
		<p class="photo"><?php topics_image (); ?></p>	
	</dd>
<?php $free = get_post_meta($post->ID,'自由入力',true);
 if($free != ''): ?>
	<dd class="content"><?php echo $free; ?></dd>
<?php endif; ?>
        </dl>
	</div><!-- .post -->



<?php
$next_post = get_next_post( );
$prev_post = get_previous_post( );
?>
	<div class="navigation clearfix">
<?php if($next_post): ?>
		<p class="next"><a href="<?php echo get_permalink($next_post -> ID); ?>" rel="next">&nbsp;<?php echo get_the_title($next_post -> ID); ?></a></p>
<?php endif; ?>
<?php if($prev_post): ?>
		<p class="previous"><a href="<?php echo get_permalink($prev_post -> ID); ?>" rel="prev"><?php echo get_the_title($prev_post -> ID); ?>&nbsp;</a></p>
<?php endif; ?>
	</div>

</div><!-- #primary -->


<?php endwhile; else: ?>
    <p>投稿がありません</p>
<?php endif; ?>

<?php get_sidebar(); ?>

		</div><!-- #main -->

	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<a href="<?php bloginfo('url'); ?>/campaign/" title="スタジオ キノシタのキャンペーン">キャンペーン</a>&nbsp;＞&nbsp;<?php echo esc_attr( $post->post_title ); ?>
	</div><!-- End: bcList -->

<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>
