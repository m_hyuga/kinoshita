<?php 

$month = date('n');
$currentyear = date('Y'); 
$year = $currentyear;
?>
	<div class="topicsList clearfix">
		<dl>
			<dt><?php echo $year; ?>年</dt>
<?php
$i = 0;
$loopcnt = 0;
while( $i < 2 ):

	//記事一個のときのループ抜け処理、2年分走査
	if($loopcnt >= 24 ){ break; }

	if($loopcnt != 0):
		if($month == 1):
			$month = 12;
			$year--;
    	else:
			$month--;
		endif;
	endif;

	$myQuery = new WP_Query(); // WP_Queryオブジェクト生成
    	$param = array( //パラメータ。
			'post_type' => 'campaign',
			'year' => $year,
			'monthnum'=> $month,
        	'posts_per_page' => '-1', //（整数）- 1ページに表示する記事数。-1 ならすべての投稿を取得。
			'post_status' => 'publish', //取得するステータスを指定：publish（公開済み）
        	'order' => 'DESC' //降順。
		);
    $myQuery->query($param);  // クエリにパラメータを渡す

	if($myQuery->have_posts()):

		if($loopcnt != 0 && $currentyear != $year): ?>
			<dt><?php echo $year; ?>年</dt>
		<?php endif; ?>
			<dd><p><?php echo $month; ?>月</p>
				<ul>

<?php while($myQuery->have_posts()) : $myQuery->the_post();

$days=14;
$today=date('U'); $entry=get_the_time('U');
$diff1=date('U',($today - $entry))/86400;
?>
        			<li<? if ($days > $diff1) { echo ' class="new"';} ?>><a href="<?php the_permalink(); ?>" title="石川県金沢市の写真館スタジオキノシタ｜<?php the_title(); ?>の詳細へ"><?php the_title(); ?></a></li>

<?php endwhile; ?>
			</ul>
		</dd>
<?php $i++; ?>     
<?php endif; ?>
<?php $loopcnt++; ?>  
<?php endwhile; ?>
	</dl>
		<div class="more"><a href="<?php site_top_url(); ?>/campaign/?show=all" title="石川県金沢市の写真館スタジオキノシタ｜以前のキャンペーン">以前のキャンペーン</a></div>
	</div>	
    
<?php ?>
