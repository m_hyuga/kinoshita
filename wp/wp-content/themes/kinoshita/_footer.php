	<div id="footer">
		<div class="siteInfo">
			<p><img src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/common/footer_txt.gif" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」｜さまざまなシーン　ひとときを　思い出に" width="962" height="19" title="石川県金沢市の写真館「フォトスタジオ キノシタ」｜さまざまなシーン　ひとときを　思い出に"></p>
			<ol>
				<li class="kids"><h3><a href="http://www.studio-kinoshita.com/kids/" title="お子様｜すくすくと成長される子供の節目・記念に☆">お子様</a>（<a href="http://www.studio-kinoshita.com/kids/shichigosan/" title="お子様の七五三の写真撮影・七五三の衣装レンタル・着付け">七五三</a>、<a href="http://www.studio-kinoshita.com/kids/omiyamairi/" title="お子様（赤ちゃん）のお宮参り・お食い初め（百日祝い）の写真撮影">お宮参り・百日祝い</a>、<a href="http://www.studio-kinoshita.com/kids/first_anniversary/" title="お子様の初誕生・1歳記念・初節句の写真撮影・１歳撮影用の貸衣装">1歳記念</a>、<a href="http://www.studio-kinoshita.com/kids/tenth_anniversary/" title="お子様の10歳記念（1/2成人式）、十三参りの写真撮影・10歳用の貸衣装">10歳記念（1/2成人式）</a>、<a href="http://www.studio-kinoshita.com/kids/ceremony/" title="お子様の卒園・入園・卒業・入学の記念写真・卒園ハカマの外出衣装レンタル">卒入園・卒入学</a>、<a href="http://www.studio-kinoshita.com/kids/birthday/" title="お子様のお誕生日・記念写真・大きめサイズの撮影用衣装レンタル">お誕生日</a>）</h3></li><br>
				<li class="grownup"><h3><a href="http://www.studio-kinoshita.com/growup/" title="成人式・卒業式写真｜新成人を祝福する「成人の日」の記念に♪">成人・卒業</a></h3></li>
				<li class="bridal"><h3><a href="http://www.studio-kinoshita.com/bridal/" title="結婚式写真・ブライダルフォト｜最高に幸せなウェディング、メモリアル記念♪">ブライダル</a></h3></li>
				<li class="memorial"><h3><a href="http://www.studio-kinoshita.com/family/" title="家族写真・記念写真｜結婚記念日や敬老の日のお祝い、マタニティフォトなど。">家族・記念日</a></h3></li>
				<li class="portrait"><h3><a href="http://www.studio-kinoshita.com/portrait/" title="ポートレート・肖像写真｜肖像・プロフィール・オーディションなど。">ポートレート・肖像写真</a></h3></li>
				<li class="certificate"><h3><a href="http://www.studio-kinoshita.com/idphoto/" title="証明写真｜第一印象に差をつける！">証明写真</a></h3></li>
			</ol>      

			<dl class="clearfix">
				<dd><h2>素敵な思い出、大切な人との絆を1枚の写真にこめて</h2></dd>
				<dt><h1>
<?php if(is_front_page()): ?>
<img src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/common/logo.gif" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」" width="280" height="78" title="石川県金沢市の写真館「フォトスタジオ キノシタ」におまかせください。">
<?php else: ?>
<a href="http://www.studio-kinoshita.com/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」におまかせください。"><img src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/common/logo.gif" alt="石川県金沢市の写真館「フォトスタジオ キノシタ」" width="280" height="78"></a>
<?php endif; ?>
</h1></dt>
				<dd class="address"><p class="tel">076-244-4649</p>
				<p>石川県金沢市平和町 2-11-11</p>
 				<p class="binsHours">営業時間:9:00〜18:00  定休日：火曜</p>
				</dd>
			</dl>

			<div class="copyright clearfix">
				<p>2013-2016 (C) <a href="http://www.studio-kinoshita.com/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」におまかせください。">石川県金沢市の写真館「フォトスタジオ キノシタ」</a> All Rights Reserved.</p>
				<ul>
					<li><a href="http://www.studio-kinoshita.com/feature/" title="石川県金沢市の写真館｜キノシタって">キノシタって</a>｜</li>
					<li><a href="http://www.studio-kinoshita.com/access/" title="石川県金沢市の写真館｜アクセス">アクセス</a>｜</li>
					<li><a href="http://www.studio-kinoshita.com/faq/" title="石川県金沢市の写真館｜よくある質問">よくある質問</a>｜</li>
					<li><a href="http://www.studio-kinoshita.com/voice/" title="石川県金沢市の写真館｜お客様の声">お客様の声</a>｜</li>
					<li><a href="https://www.studio-kinoshita.com/contact/" title="石川県金沢市の写真館｜お問い合わせ">お問い合わせ</a>｜</li>
					<li><a href="http://www.studio-kinoshita.com/copyright/" title="石川県金沢市の写真館｜著作権について">著作権について</a>｜</li>
					<li><a href="http://www.studio-kinoshita.com/privacy/" title="石川県金沢市の写真館｜プライバシーポリシー">プライバシーポリシー</a>｜</li>
					<li><a href="http://www.studio-kinoshita.com/sitemap/" title="石川県金沢市の写真館｜サイトマップ">サイトマップ</a></li>
				</ul>
			</div>
	  </div><!-- .siteInfo -->
	</div><!-- #footer -->

</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>