<?php 
	/* Template Name: ブライダル衣装 */
	/*wp_enqueue_style("common_1clm_css", get_bloginfo('template_directory').'/css/common_1column.css' );*/
wp_enqueue_style("lightboxcss", get_bloginfo('template_directory').'/css/gallery/jquery.lightbox-0.5.css' );
wp_enqueue_script("lightboxjs",get_bloginfo('template_directory').'/js/gallery/jquery.lightbox-0.5.js' );
	$pageColumn = 1;
	$pageName = dress;
	$base_dir = '/home/sites/heteml/users/k/i/n/kino3/web/images/';
	get_header();
?>

	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<a href="<?php bloginfo('url'); ?>/bridal/" title="結婚式写真・ブライダルフォト｜最高に幸せなウェディング、メモリアル記念♪">結婚式写真・ブライダルフォト</a>&nbsp;＞&nbsp;
		衣装
	</div><!-- End: bcList -->

	<div id="main" class="Column1 dress bridalDress">
    
<?php the_post(); ?>

		<h3 title="結婚式・ブライダル衣装">結婚式・ブライダル衣装</h3>


		<div class="entry post-<?php the_ID(); ?>">
			<ol class="clearfix">
				<li><a href="#dress01" title="結婚式・ブライダル衣装｜ウェディングドレス"><img src="<?php echo get_bloginfo('template_directory') ?>/images/bridal/dress/icon_01.gif" width="134" height="30" alt="結婚式・ブライダル衣装｜ウェディングドレス"></a></li>
				<li><a href="#dress02" title="結婚式・ブライダル衣装｜色内掛"><img src="<?php echo get_bloginfo('template_directory') ?>/images/bridal/dress/icon_02.gif" width="134" height="30" alt="結婚式・ブライダル衣装｜色内掛"></a></li>
				<li><a href="#dress03" title="結婚式・ブライダル衣装｜カラードレス"><img src="<?php echo get_bloginfo('template_directory') ?>/images/bridal/dress/icon_03.gif" width="134" height="30" alt="結婚式・ブライダル衣装｜カラードレス"></a></li>
				<li><a href="#dress04" title="結婚式・ブライダル衣装｜タキシード"><img src="<?php echo get_bloginfo('template_directory') ?>/images/bridal/dress/icon_04.gif" width="134" height="30" alt="結婚式・ブライダル衣装｜タキシード"></a></li>
			</ol>        

		    <div id="dress01" class="category">
			<h4><img src="<?php echo get_bloginfo('template_directory') ?>/images/bridal/dress/sttl_01.gif" width="235" height="57" alt="結婚式・ブライダル衣装｜ウェディングドレス" title="結婚式・ブライダル衣装｜ウェディングドレス"></h4>
			<ul class="clearfix">

<?php $num = count(glob($base_dir."bridal_dress/00/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
					<a href="/images/bridal_dress/00/00_<?php echo $i; ?>.jpg" rel="lightbox[00]" title="結婚式・ブライダル衣装｜ウェディングドレス">
						<img src="/images/bridal_dress/00/00_<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>
			</ul>
		    </div><!-- /#dress01 -->

  

		    <div id="dress02" class="category">
			<h4><img src="<?php echo get_bloginfo('template_directory') ?>/images/bridal/dress/sttl_02.gif" width="235" height="57" alt="結婚式・ブライダル衣装｜色内掛" title="結婚式・ブライダル衣装｜色内掛"></h4>
			<ul class="clearfix">

<?php $num = count(glob($base_dir."bridal_dress/01/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
					<a href="/images/bridal_dress/01/01_<?php echo $i; ?>.jpg" rel="lightbox[01]" title="結婚式・ブライダル衣装｜色内掛">
						<img src="/images/bridal_dress/01/01_<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>
			</ul>
		    </div><!-- /#dress02 -->

	<div class="pagetop">
		<a href="#page" title="結婚式・ブライダル衣装｜ページTOPへ"><img src="<?php echo get_bloginfo('template_directory') ?>/images/common/pagetop.gif" alt="結婚式・ブライダル衣装｜ページTOPへ" width="114" height="42"></a>
	</div>    

		    <div id="dress03" class="category">
			<h4><img src="<?php echo get_bloginfo('template_directory') ?>/images/bridal/dress/sttl_03.gif" width="235" height="57" alt="結婚式・ブライダル衣装｜カラードレス" title="結婚式・ブライダル衣装｜カラードレス"></h4>
			<ul class="clearfix">

<?php $num = count(glob($base_dir."bridal_dress/02/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
					<a href="/images/bridal_dress/02/02_<?php echo $i; ?>.jpg" rel="lightbox[02]" title="結婚式・ブライダル衣装｜カラードレス">
						<img src="/images/bridal_dress/02/02_<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>
			</ul>
		    </div><!-- /#dress03 -->
   

		    <div id="dress04" class="category">
			<h4><img src="<?php echo get_bloginfo('template_directory') ?>/images/bridal/dress/sttl_04.gif" width="235" height="57" alt="結婚式・ブライダル衣装｜タキシード" title="結婚式・ブライダル衣装｜タキシード"></h4>
			<ul class="clearfix">

<?php $num = count(glob($base_dir."bridal_dress/03/*b.jpg")); ?>
<?php for($i=1; $i<=$num; $i++): ?>
<?php if($i < 10) { $i = '0'.$i; } ?>
				<li>
					<a href="/images/bridal_dress/03/03_<?php echo $i; ?>.jpg" rel="lightbox[03]" title="結婚式・ブライダル衣装｜タキシード">
						<img src="/images/bridal_dress/03/03_<?php echo $i; ?>_thumb.jpg" />
					</a>
				</li>
<?php endfor; ?>
			</ul>
		    </div><!-- /#dress04 -->

		</div>		
	</div>


	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<a href="<?php bloginfo('url'); ?>/bridal/" title="結婚式写真・ブライダルフォト｜最高に幸せなウェディング、メモリアル記念♪">結婚式写真・ブライダルフォト</a>&nbsp;＞&nbsp;
		衣装
	</div><!-- End: bcList -->


<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>