<?php 
	/* Template Name: お客様の声 */
	wp_enqueue_style("voice_css", get_bloginfo('template_directory').'/css/voice.css' );
	$pageColumn = 2;
	$pageName = 'voice';
	if( $_REQUEST['vcat']){
		$cat= $_REQUEST['vcat'];
	}
	get_header();
?>

	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="http://www.studio-kinoshita.com/" title="石川県金沢市の写真館スタジオキノシタ｜トップ">トップ</a>&nbsp;＞&nbsp;
		お客様の声
	</div><!-- End: bcList -->

	<div id="main" class="<?php echo $pageName; ?> wrapper Column2">
		<div id="primary" class="site-content">

<?php the_post(); ?>
			<h3 title="お客様の声"><span>お客様の声</span></h3>

			<ul class="floatList clearfix cateMenu">
				<li><a href="./" title="お客様の声｜全体"><img alt="お客様の声｜全体" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/voice/icon_01.gif" /></a></li>
				<li><a href="./?vcat=02" title="お客様の声｜お子様"><img alt="お客様の声｜お子様" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/voice/icon_02.gif" /></a></li>
				<li><a href="./?vcat=03" title="お客様の声｜成人・卒業"><img alt="お客様の声｜成人・卒業" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/voice/icon_03.gif" /></a></li>
				<li><a href="./?vcat=04" title="お客様の声｜ブライダル"><img alt="お客様の声｜ブライダル" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/voice/icon_04.gif" /></a></li>
				<li><a href="./?vcat=05" title="お客様の声｜家族・記念日"><img alt="お客様の声｜家族・記念日" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/voice/icon_05.gif" /></a></li>
				<li><a href="./?vcat=06" title="お客様の声｜ポートレート"><img alt="お客様の声｜ポートレート" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/voice/icon_06.gif" /></a></li>
				<li><a href="./?vcat=07" title="お客様の声｜証明写真"><img alt="お客様の声｜証明写真" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/voice/icon_07.gif" /></a></li>
				<li><a href="./?vcat=08" title="お客様の声｜その他"><img alt="お客様の声｜その他" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/voice/icon_08.gif" /></a></li>

			</ul>


			<div class="entry post-<?php the_ID(); ?>">
				<?php the_content(); ?>


<?php
$photo_Cate["お子様"] = "02";
$photo_Cate["成人・卒業"] = "03";
$photo_Cate["ブライダル"] = "04";
$photo_Cate["家族・記念日"] = "05";
$photo_Cate["ポートレート"] = "06";
$photo_Cate["証明写真"] = "07";
$photo_Cate["その他"] = "08";

$paged = get_query_var('paged'); //現在のページ番号取得
if($cat):
$wp_query = new WP_Query(array(
	'meta_query' => array(
		array(	'ka'=>'カテゴリ',
		'value'=>array_search($cat, $photo_Cate)
		)
	),
'post_type' => 'voice',
'posts_per_page' => '12',
'paged' => $paged,
'orderby' => 'date',
'order' => 'DESC'
));
else:
$wp_query = new WP_Query(array(
'post_type' => 'voice',
'posts_per_page' => '12',
'paged' => $paged,
'orderby' => 'date',
'order' => 'DESC'
));
endif;

$ListCnt = 0;

if ($wp_query->have_posts()) :
	while($wp_query->have_posts()) : $wp_query->the_post(); ?>

      <!-- Start: Post -->
      <?php if($ListCnt == 0) echo '<ul class="floatList clearfix photoList">'; ?>
      <li>
	<dl>
		<dt class="<?php echo voice_eyechatch_size ('アイキャッチ'); ?>"><a href="<?php the_permalink(); ?>" title="<?php echo get_post_meta( $post->ID , 'お住まいのエリア' , ture ); ?>　<?php the_title(); ?> 様"><?php voice_eyechatch_image (); ?></a></dt>
		<dd><img alt="<?php echo get_post_meta( $post->ID , 'カテゴリ' , ture ); ?>" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/voice/icon_<?php echo $photo_Cate[get_post_meta( $post->ID , 'カテゴリ' , ture )]; ?>.gif" /></dd>
		<dd><?php echo get_post_meta( $post->ID , 'お住まいのエリア' , ture ); ?>　<?php the_title(); ?> 様</dd>
	</dl>
      </li>
      <?php if(isLast($wp_query)){
		echo '</ul>';
		$ListCnt = 0;
	}else{
		if($ListCnt == 2){
			echo '</ul>';
			$ListCnt = 0;
		}else{$ListCnt ++;}
	}
      ?>

    <!-- End: Post -->

<?php endwhile; ?>
	<div class="navigation clearfix">
		<p class="next"><?php next_posts_link('&nbsp;次のページ'); ?></p>
		<p class="previous"><?php previous_posts_link('前のページ&nbsp;'); ?></p>
	</div>
<?php endif; ?>
<?php wp_reset_query(); ?>


			</div>


<?php get_template_part( 'primary_footer' ); ?>

		</div><!-- #primary -->

<?php get_sidebar(); ?>

	
	</div><!-- #main .wrapper -->



	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="http://www.studio-kinoshita.com/" title="石川県金沢市の写真館スタジオキノシタ｜トップ">トップ</a>&nbsp;＞&nbsp;
		お客様の声
	</div><!-- End: bcList -->


<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>