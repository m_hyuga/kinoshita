<?php
function trust_form_show_input() {
	global $trust_form;
	$col_name = $trust_form->get_col_name();
	$validate = $trust_form->get_validate();
	$config = $trust_form->get_config();
	$attention = $trust_form->get_attention();
	
	$nonce = wp_nonce_field('trust_form','trust_form_input_nonce_field');

	$html = <<<EOT
<div id="trust-form" class="contact-form contact-form-input" >
<p id="message-container-input">{$trust_form->get_input_top()}</p>
<form action="#trust-form" method="post" >
<table>
<tbody>
EOT;

	foreach ( $col_name as $key => $name ) {
		if ( $key == 'element-0' ){
			$html .= '<tr><th scope="row"><div class="subject"><i class="icon-star-empty"></i><span class="content">'.$name.'</span>'.(isset($validate[$key]['required']) && $validate[$key]['required'] == 'true' ? '<span class="require">'.$config['require'].'</span>' : '' );
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
			$html .= '</div><div class="submessage">'.$attention[$key].'</div></th></tr><tr><td><div class="menu clearfix">'.$trust_form->get_element( $key ).'</div>';
			$html .= '</td></tr>';
	            continue;

		} elseif ( $key == 'element-1' ){
			$html .= '<tr><th scope="row"><div class="subject"><span class="content"><i class="icon-star-empty"></i>'.$name.'</span>'.(isset($validate[$key]['required']) && $validate[$key]['required'] == 'true' ? '<span class="require">'.$config['require'].'</span>' : '' );
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
			$html .= '</div><div class="submessage">'.$attention[$key].'</div></th></tr><tr><td><div class="reserve clearfix">'.$trust_form->get_element( $key ).'</div>';
			$html .= '</td></tr>';
		            continue;

		} elseif ( $key == 'element-2' ){
			$html .= '<tr><th scope="row"><div class="subject"><span class="content"><i class="icon-star-empty"></i>'.$name.'</span>'.(isset($validate[$key]['required']) && $validate[$key]['required'] == 'true' ? '<span class="require">'.$config['require'].'</span>' : '' );
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
			$html .= '</div><div class="submessage red">'.$attention[$key].'</div></th></tr><tr><td><div>'.$trust_form->get_element( $key ).'</div>';
			$html .= '<p class="example">記入例）<br>12/24が娘の誕生日なのでこの日に予約を考えています。<br>人見知りがあり午後にお昼寝もするため、何時だとあいていますか？<br><br>「予約している」場合<br>12月21日に予約しています。<br>人見知りがあり午後にお昼寝もするため、当日の時間変更は可能ですか？</p>';

			$html .= '</td></tr></tbody></table>';
		            continue;

		} elseif ( $key == 'element-3' ){
			$html .= '<table class="customer"><tbody><tr><th colspan="3" scope="row" class="thead"><div class="subject"><span class="content"><i class="icon-star-empty"></i>お客様についてご記入ください</span><span class="require">（※必須）</span></div></th></tr>';
			$html .= '<tr><th scope="row"><div class="subcontent">'.$attention[$key].'</div></th><td class="tleft name"><div><span class="content-name">'.$name.'</span>'.$trust_form->get_element( $key ).'</div>';
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
			$html .= '</td>';
		            continue;

		} elseif ( $key == 'element-4' ){
			$html .= '<td class="name"><div><span class="content-name">'.$name.'</span>'.$trust_form->get_element( $key ).'</div>';
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
			$html .= '</td>';
		            continue;

		} elseif ( $key == 'element-5' ){
			$html .= '<tr><th scope="row"><div class="subcontent">'.$attention[$key].'</div></th><td class="name"><div><span class="content-name">'.$name.'</span>'.$trust_form->get_element( $key ).'</div>';
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
			$html .= '</td>';
		            continue;

		} elseif ( $key == 'element-6' ){
			$html .= '<td class="name"><div><span class="content-name">'.$name.'</span>'.$trust_form->get_element( $key ).'</div>';
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
			$html .= '</td>';
		            continue;

		} elseif ( $key == 'element-7' ){
			$html .= '<tr><th scope="row"><span class="subcontent">'.$name.'</span></th><td colspan="2" class="pl2em"><div>'.$trust_form->get_element( $key ).'<span class="submessage">'.$attention[$key].'</span></div>';
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
			$html .= '</td>';
		            continue;

		} elseif ( $key == 'element-8' ){
			$html .= '<tr><th scope="row"><span class="subcontent">'.$name.'</span></th><td colspan="2" class="pl2em"><div>'.$trust_form->get_element( $key ).'<span class="submessage">'.$attention[$key].'</span></div>';
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
		            continue;

		} elseif ( $key == 'element-9' ){
			$html .= '<div class="sbox"><span class="subcontent">ご連絡可能な曜日と時間帯</span>'.$trust_form->get_element( $key ).'<span class="submessage">'.$attention[$key].'</span></div>';
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
		            continue;

		} elseif ( $key == 'element-10' ){
			$html .= '<div class="clearfix dow">'.$trust_form->get_element( $key ).'</div>';
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
			$html .= '<div class="submessage">'.$attention[$key].'</div></td></tr>';
		            continue;

		} elseif ( $key == 'element-11' ){
			$html .= '<tr><th scope="row"><span class="subcontent">'.$name.'</span></th><td colspan="2" class="pl2em"><div>'.$trust_form->get_element( $key ).'<span class="submessage">'.$attention[$key].'</span></div>';
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
			$html .= '</td>';
		            continue;

		} elseif ( $key == 'element-12' ){
			$html .= '<tr><th scope="row"><span class="subcontent">'.$name.'</span><div class="submessage">'.$attention[$key].'</div></th><td colspan="2" class="pl2em customer-adr"><div class="sbox">'.$trust_form->get_element( $key ).'</div>';
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
		            continue;

		} elseif ( $key == 'element-14' ){
			$html .= '<div class="adr"><span class="content">'.$name.'</span>'.$trust_form->get_element( $key ).'</div><div class="submessage">'.$attention[$key].'</div>';
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}		
		            continue;

		} elseif ( $key == 'element-15' ){
			$html .= '<div class="adr"><span class="content">'.$name.'</span>'.$trust_form->get_element( $key ).'</div><div class="submessage">'.$attention[$key].'</div>';
			$err_msg = $trust_form->get_err_msg($key);
			if ( isset($err_msg) && is_array($err_msg) ) {
				$html .= '<div class="error">';
				foreach ( $err_msg as $msg ) {
					$html .= $msg;
				}
				$html .= '</div>';
			}
			$html .= '<p>お客さまの個人情報を安全に送受信するためにSSL（暗号化送信）技術を使用しています。<br>ご入力いただいた個人情報は、お問い合わせに関するご回答の目的でのみ利用させていただきます。<br><br>個人情報の取扱につきましてはこちらをご覧ください。→ <a href="http://www.studio-kinoshita.com/privacy/">スタジオキノシタ　プライバシーポリシー</a></p>';
			$html .= '</td></tr>';
		            continue;

		} else {
		$html .= '<tr><th scope="row"><div class="subject"><span class="content"><i class="icon-star-empty"></i>'.$name.'</span>'.(isset($validate[$key]['required']) && $validate[$key]['required'] == 'true' ? '<span class="require">'.$config['require'].'</span>' : '' ).'</div><div class="submessage">'.$attention[$key].'</div></th><td><div>'.$trust_form->get_element( $key ).'</div>';
		}
		
		$err_msg = $trust_form->get_err_msg($key);
		if ( isset($err_msg) && is_array($err_msg) ) {
			$html .= '<div class="error">';
			foreach ( $err_msg as $msg ) {
				$html .= $msg.'<br />';
			}
			$html .= '</div>';
		}
		$html .= '</td></tr>';
	}
	$html .= <<<EOT
</tbody>
</table>
{$nonce}


<div class="confirm-btn-area">
<p id="confirm-button" class="submit-container">{$trust_form->get_form('input_bottom')}
<a href="JavaScript:document.forms[0].reset()"><img src="https://ssl.webhosting-jp.com/kinoshita/wp-content/themes/kinoshita/images/contact/cancel_btn.gif" border="0" alt="キャンセル"></a></p>
</div>
</form>
</div>
EOT;

	return $html;
}



function trust_form_show_confirm() {
	global $trust_form;
	$col_name = $trust_form->get_col_name();

	$nonce = wp_nonce_field('trust_form','trust_form_confirm_nonce_field');

	$html = <<<EOT
<div id="trust-form" class="contact-form contact-form-confirm" >
<p id="message-container-confirm">{$trust_form->get_form('confirm_top')}</p>
<form action="#trust-form" method="post" >
<table>
<tbody>
EOT;
	foreach ( $col_name as $key => $name ) {
		if ( $key == 'element-3' ){
			$html .= '<tr><th><div class="subject"><i class="icon-star-empty"></i>お客様についてご記入ください</div></th></tr>';
			$html .= '<tr><td><table class="customer"><tr><th>お名前</th><td><div><span>姓　　</span>'.$trust_form->get_input_data($key).'</div></td>';
		            continue;
		}
		if ( $key == 'element-4' ){
			$html .= '<td><div><span>名　　</span>'.$trust_form->get_input_data($key).'</div></td></tr>';
		            continue;
		}
		if ( $key == 'element-5' ){
			$html .= '<tr><th>ふりがな</th><td><div><span>姓　　</span>'.$trust_form->get_input_data($key).'</div></td>';
		            continue;
		}
		if ( $key == 'element-6' ){
			$html .= '<td><div><span>名　　</span>'.$trust_form->get_input_data($key).'</div></td></tr>';
		            continue;
		}
		if ( $key == 'element-7' ||  $key == 'element-8' ||  $key == 'element-9' ||  $key == 'element-10' ||  $key == 'element-11' ){
			$html .= '<tr><th>'.$name.'</th><td colspan="2"><div>'.$trust_form->get_input_data($key).'</div></td></tr>';
		            continue;
		}
		if ( $key == 'element-12' ){
			$html .= '<tr><th>'.$name.'</th><td colspan="2" class="adr"><div><span>都道府県名　　</span>'.$trust_form->get_input_data($key).'</div>';
		            continue;
		}
		if ( $key == 'element-14' ){
			$html .= '<div><span>'.$name.'　　</span>'.$trust_form->get_input_data($key).'</div>';
		            continue;
		}
		if ( $key == 'element-15' ){
			$html .= '<div><span>'.$name.'　　</span>'.$trust_form->get_input_data($key).'</div></td>';
			$html .= '</tr></table></td></tr>';
		            continue;
		}

		$html .= '<tr><th><div class="subject"><i class="icon-star-empty"></i>'.$name.'</div></th></tr><tr><td><div>'.$trust_form->get_input_data($key).'</div>';
		$html .= '</td></tr>';
	}
	$html .= <<<EOT
</tbody>
</table>
{$nonce}
<p id="confirm-button" class="submit-container">{$trust_form->get_form('confirm_bottom')}</p>
</form>
</div>
EOT;
	return $html;
}



function trust_form_show_finish() {
	global $trust_form;
	
	$html = <<<EOT
<div id="trust-form" class="contact-form contact-form-finish" >
<p id="message-container-confirm">{$trust_form->get_form('finish')}</p>
</div>
EOT;
	return $html;
}
?>