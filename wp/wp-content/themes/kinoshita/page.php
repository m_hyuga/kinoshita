<?php get_header();?>
<div class="main">
  <div class="breadcrumbs"> <!-- パンくずナビ -->
    <?php if(function_exists('bcn_display')){bcn_display();}?>
  </div>
  <?php the_post(); ?>
  <h2><?php the_title(); ?></h2>      <!-- ページタイトル  -->
  <div class="entry post-<?php the_ID(); ?>">
    <?php the_content(); ?>         <!-- ページのコンテンツ  -->
  </div>		
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>