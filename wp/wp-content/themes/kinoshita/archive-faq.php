<?php 
	/* Template Name: よくある質問 */
	wp_enqueue_style("faq_css", get_bloginfo('template_directory').'/css/faq.css' );
	$pageColumn = 2;
	$pageName = 'faq';
	if( $_REQUEST['fcat']){
		$cat= $_REQUEST['fcat'];
	}
	get_header();
?>
	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="http://www.studio-kinoshita.com/" title="石川県金沢市の写真館スタジオキノシタ｜トップ">トップ</a>&nbsp;＞&nbsp;
		よくある質問
	</div><!-- End: bcList -->

	<div id="main" class="faq wrapper Column2">
		<div id="primary" class="site-content">

<?php the_post(); ?>
			<h3 title="よくある質問"><span>よくある質問</span></h3>

			<ul class="floatList clearfix cateMenu">
				<li><a href="./" title=" title="よくある質問｜全体""><img alt="よくある質問｜全体" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/faq/icon_01.gif" /></a></li>
				<li><a href="./?fcat=02" title="よくある質問｜予約について"><img alt="よくある質問｜予約について" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/faq/icon_02.gif" /></a></li>
				<li><a href="./?fcat=03" title="よくある質問｜衣装について"><img alt="よくある質問｜衣装について" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/faq/icon_03.gif" /></a></li>
				<li><a href="./?fcat=04" title="よくある質問｜撮影について"><img alt="よくある質問｜撮影について" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/faq/icon_04.gif" /></a></li>
				<li><a href="./?fcat=05" title="よくある質問｜仕上りについて"><img alt="よくある質問｜仕上りについて" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/faq/icon_05.gif" /></a></li>
				<li><a href="./?fcat=06" title="よくある質問｜その他"><img alt="よくある質問｜その他" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/faq/icon_06.gif" /></a></li>

			</ul>

			<div class="entry post-<?php the_ID(); ?>">
				<!-- <?php the_content(); ?> -->

<?php
$faq_Cate["予約について"] = "02";
$faq_Cate["衣装について"] = "03";
$faq_Cate["撮影について"] = "04";
$faq_Cate["仕上がりについて"] = "05";
$faq_Cate["その他"] = "06";

$paged = get_query_var('paged'); //現在のページ番号取得
if($cat):
$wp_query = new WP_Query(array(
	'meta_query' => array(
		array(	'ka'=>'カテゴリ',
		'value'=>array_search($cat, $faq_Cate)
		)
	),
'post_type' => 'faq',
'posts_per_page' => '10',
'paged' => $paged,
'orderby' => 'date',
'order' => 'DESC'
));
else:
$wp_query = new WP_Query(array(
'post_type' => 'faq',
'posts_per_page' => '10',
'paged' => $paged,
'orderby' => 'date',
'order' => 'DESC'
));
endif;

if ($wp_query->have_posts()) :
	while($wp_query->have_posts()) : $wp_query->the_post(); ?>

      <!-- Start: Post -->
	<dl>
		<dt class="clearfix">
			<p><img alt="<?php echo get_post_meta( $post->ID , 'カテゴリ' , ture ); ?>" src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/faq/icon_<?php echo $faq_Cate[get_post_meta( $post->ID , 'カテゴリ' , ture )]; ?>.gif"  title="<?php echo get_post_meta( $post->ID , 'カテゴリ' , ture ); ?>" /></p>
			<h4><?php the_title(); ?></h4></dt>
		<dd><?php the_content(); ?></dd>
	</dl>
      <!-- End: Post -->

	<?php endwhile; ?>
	<div class="navigation clearfix">
		<p class="next"><?php next_posts_link('&nbsp;次のページ'); ?></p>
		<p class="previous"><?php previous_posts_link('前のページ&nbsp;'); ?></p>
	</div>
<?php endif; ?>
<?php wp_reset_query(); ?>


			</div>


			<div class="solution_2clm clearfix">
				<dl>
					<dt><img src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/faq/sttl01.gif" width="620" height="24" alt="よくある質問｜解決できなかった方は" title="よくある質問｜解決できなかった方は"></dt>
					<dd class="contact">
<p>メールでのお問い合わせ</p>
<a href="https://www.studio-kinoshita.com/contact/" title="よくある質問｜解決できなかった方は、メールでお問い合わせ"><img src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/faq/btn_01.gif" width="284" height="104" alt="よくある質問｜解決できなかった方は、メールでお問い合わせ" border="0"></a></dd>
					<dd class="faq">
<p>お電話でのお問い合わせ</p>
<img src="http://www.studio-kinoshita.com/wp-content/themes/kinoshita/images/faq/btn_02.gif" width="284" height="104" alt="よくある質問｜解決できなかった方は、お電話でのお問い合わせ 076 - 244 - 4649 9:00~18:00 火曜定休" title="よくある質問｜解決できなかった方は、お電話でのお問い合わせ 076 - 244 - 4649 9:00~18:00 火曜定休" border="0"></dd>
				</dl>
			</div>

		</div><!-- #primary -->

<?php get_sidebar(); ?>

	
	</div><!-- #main .wrapper -->


	<!-- Start: bcList -->
	<div class="bcList">
		■ 現在位置 : <a href="http://www.studio-kinoshita.com/" title="石川県金沢市の写真館スタジオキノシタ｜トップ">トップ</a>&nbsp;＞&nbsp;
		よくある質問
	</div><!-- End: bcList -->


<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>