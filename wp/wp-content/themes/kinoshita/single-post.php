<?php
	wp_enqueue_style("topics_campaign_css", get_bloginfo('template_directory').'/css/topics_campaign.css' );
	$pageName = 'topics';
	$topics = 'topics_single';
get_header(); ?>

	<!-- Start: bcList -->
	<div class="bcList">
		<a href="<?php bloginfo('url'); ?>/" title="石川県金沢市の写真館「フォトスタジオ キノシタ」">トップ</a>&nbsp;＞&nbsp;
		<a href="<?php bloginfo('url'); ?>/news/" title="フォトスタジオ キノシタのトピックス">トピックス</a>&nbsp;＞&nbsp;<?php echo esc_attr( $post->post_title ); ?>
	</div><!-- End: bcList -->

<div id="main" class="wrapper Column2 topics">
  <div id="primary">

	<h3 title="フォトスタジオ キノシタのトピックス"><span>トピックス</span></h3>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div class="post">
        <dl>
        <dt><?php the_title(); ?></dt>
        <dd class="date"><?php the_time('Y年m月d日'); ?></dd>
	<dd><p class="read"><?php echo get_post_meta($post->ID,'リード文',true); ?></p>
		<p class="photo"><?php topics_image (); ?></p>	
	</dd>
	<dd class="content"><?php echo get_the_content(); ?></dd>
        </dl>
	</div><!-- .post -->

	<div class="navigation clearfix">
		<p class="next"><?php next_post_link( '%link', '&nbsp;次のトピック', TRUE ); ?></p>
		<p class="previous"><?php previous_post_link( '%link', '前のトピック&nbsp;', TRUE ); ?></p>	
	</div>

</div><!-- #primary -->


<?php endwhile; else: ?>
    <p>投稿がありません</p>
<?php endif; ?>

<?php get_sidebar(); ?>

		</div><!-- #main -->

<?php get_template_part( 'sub_footer' ); ?>
<?php get_footer(); ?>
